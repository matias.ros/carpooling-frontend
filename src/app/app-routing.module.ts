import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/seguridad/login/login.component";
import { HomeComponent } from "./components/common/home/home.component";
import { MapaComponent } from "./components/administracion/mapa/mapa.component";
import { VerificacionEmailComponent } from "./components/administracion/registrar-usuario/verificacion-email/verificacion-email.component";
import { SeleccionarTipoComponent } from "./components/administracion/registrar-usuario/seleccionar-tipo/seleccionar-tipo.component";
import { SeleccionarNombreComponent } from "./components/administracion/registrar-usuario/seleccionar-nombre/seleccionar-nombre.component";
import { SeleccionarNacimientoComponent } from "./components/administracion/registrar-usuario/seleccionar-nacimiento/seleccionar-nacimiento.component";
import { CertificarTelefonoComponent } from "./components/administracion/registrar-usuario/certificar-telefono/certificar-telefono.component";
import { SeleccionarGeneroComponent } from "./components/administracion/registrar-usuario/seleccionar-genero/seleccionar-genero.component";
import { IngresarContraseniaComponent } from "./components/administracion/registrar-usuario/ingresar-contrasenia/ingresar-contrasenia.component";
import { DefinirTrayectoComponent } from "./components/administracion/publicar-viajes/definir-trayecto/definir-trayecto.component";
import { PerfilUsuarioComponent } from "./components/administracion/cuenta-usuario/perfil-usuario/perfil-usuario.component";
import { DefinirAportacionComponent } from "./components/administracion/publicar-viajes/definir-aportacion/definir-aportacion.component";
import { PublicarConfirmacionComponent } from "./components/administracion/publicar-viajes/publicar-confirmacion/publicar-confirmacion.component";
import { DefinirFiltrosComponent } from "./components/administracion/buscar-viajes/definir-filtros/definir-filtros.component";
import { InfoCochesComponent } from "./components/administracion/cuenta-usuario/info-coches/info-coches.component";
import { PreferenciasComponent } from "./components/administracion/cuenta-usuario/preferencias/preferencias.component";
import { InfoPersonalComponent } from "./components/administracion/cuenta-usuario/info-personal/info-personal.component";
import { CerficacionesComponent } from "./components/administracion/cuenta-usuario/cerficaciones/cerficaciones.component";
import { ListadoViajesComponent } from './components/administracion/buscar-viajes/listado-viajes/listado-viajes.component';
import { DetallesViajeComponent } from './components/administracion/buscar-viajes/detalles-viaje/detalles-viaje.component';
import { PerfilConductorComponent } from './components/administracion/buscar-viajes/perfil-conductor/perfil-conductor.component';
import { ReservarViajeComponent } from './components/administracion/buscar-viajes/reservar-viaje/reservar-viaje.component';
import { SolicitudesComponent } from './components/administracion/cuenta-usuario/solicitudes/solicitudes.component';
import { MisviajesComponent } from './components/administracion/cuenta-usuario/misviajes/misviajes.component';


const routes: Routes = [
  { path: "", component: HomeComponent, pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "mapa", component: MapaComponent },
  { path: "registro", component: SeleccionarTipoComponent },
  { path: "registro/email", component: VerificacionEmailComponent },
  { path: "registro/nombre", component: SeleccionarNombreComponent },
  { path: "registro/nacimiento", component: SeleccionarNacimientoComponent },
  { path: "registro/telefono", component: CertificarTelefonoComponent },
  { path: "registro/genero", component: SeleccionarGeneroComponent },
  { path: "registro/pass", component: IngresarContraseniaComponent },
  { path: "publicar", component: DefinirTrayectoComponent },
  { path: "publicar/confirmacion", component: PublicarConfirmacionComponent },
  { path: "buscar", component: DefinirFiltrosComponent },
  {
    path: "perfil",
    component: PerfilUsuarioComponent,
    children: [
      { path: "coches", component: InfoCochesComponent },
      { path: "informacion-personal", component: InfoPersonalComponent },
      { path: "preferencias", component: PreferenciasComponent },
      { path: "certificaciones", component: CerficacionesComponent }
    ]
  },
  { path: "buscar/viajes", component: ListadoViajesComponent },
  { path: "buscar/viajes/:id/detalles", component: DetallesViajeComponent },
  { path: "buscar/viajes/:id/conductor", component: PerfilConductorComponent },
  { path: "buscar/viajes/:id/reservar", component: ReservarViajeComponent }, 
  { path: "solicitudes", component: SolicitudesComponent }, 
  { path: "viajespublicados", component: MisviajesComponent },    
  // Ejemplo de Guard, se utiliza el AuthGuard para sitios no publicos, asi redirige al login
  // { path: 'administracion/resetear-contraseña', component: ResetearPasswordComponent, canActivate: [AuthGuard],
  //   data: {
  //     breadcrumb: 'Resetear Contraseña'
  //   }},
  // { path: 'login', component: LoginComponent },
  //rutas para cuando no esta disponible
  // {path: '**', component: NoDisponibleComponent},
  // {path: 'portal', component: NoDisponibleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
