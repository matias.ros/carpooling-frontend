import { Component } from "@angular/core";
import { ANALYTICS_KEY, FACEBOOK_APP_ID } from "./config/constants";
import { FacebookService, InitParams } from "ngx-facebook";
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError
} from "@angular/router";
import { HEADERWHITE } from "../app/config/constants";
// Service
import { DynamicscriptloaderService } from "../app/services/dynamicscriptloader.service";

declare let ga: any;
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "carpool-frontend";
  currentUser: any;
  nombre: string;
  apellido: string;
  fotoPerfilFB: string;
  loggedIn: string = "false";
  style_header: boolean = false;

  private loadScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.dynamicScriptLoader
      .load("jarallax-element", "jarallax", "theme")
      .then(data => {
        // Script Loaded Successfully
        console.log(data);
      })
      .catch(error => console.log(error));
  }
  constructor(
    private fb: FacebookService,
    public router: Router,
    private dynamicScriptLoader: DynamicscriptloaderService
  ) {
    let initParams: InitParams = {
      appId: FACEBOOK_APP_ID,
      status: true,
      cookie: true,
      xfbml: true,
      version: "v3.2"
    };

    fb.init(initParams);
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        var menu_movil = document.getElementById("navigation-menu");
        var boton_movil = document.getElementById("button-menumovil");

        this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
        if (sessionStorage.getItem("loggedIn")) {
          this.loggedIn = sessionStorage.getItem("loggedIn");
        }

        if (this.currentUser) {
          this.nombre = this.currentUser.nombre;
          this.apellido = this.currentUser.apellido;
          this.fotoPerfilFB = this.currentUser.foto;
        }
        if (event.url) {
          if (menu_movil) {
            if (menu_movil.classList.contains("show")) {
              boton_movil.click();
            }
          }

          this.style_header = false;
          HEADERWHITE.forEach(element => {
            if (event.url.includes(element)) {
              this.style_header = true;
            }
          });
          console.log(this.style_header);
        }
        this.loadScripts();
        console.log("LOADED");
      }
    });
  }
  registrarCoche() {
    this.router.navigate(["/perfil/coches"]);
  }
  ngOnInit() {
    ga("create", ANALYTICS_KEY, "auto");
  }
}
