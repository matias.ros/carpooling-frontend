import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import {DatePipe, registerLocaleData} from '@angular/common';
import localePy from '@angular/common/locales/es-PY';
registerLocaleData(localePy, 'es');
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxIntlTelInputModule } from "ngx-intl-tel-input";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { CommonModule } from "@angular/common";
import { ToastrModule } from "ngx-toastr";
import {
  NgxUiLoaderModule,
  NgxUiLoaderConfig,
  SPINNER,
  POSITION,
  PB_DIRECTION
} from "ngx-ui-loader";

import { AppComponent } from "./app.component";

import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import {ErrorInterceptor} from './util/auth-error.interceptor';
import { AuthInterceptor } from "./util/auth.interceptor";
import { AuthGuardService } from "./util/auth-guard.service";
import { HomeComponent } from "./components/common/home/home.component";
import { HeaderComponent } from "./components/common/header/header.component";
import { FooterComponent } from "./components/common/footer/footer.component";
import { LoginComponent } from "./components/seguridad/login/login.component";

import { FacebookModule } from "ngx-facebook";
import { MapaComponent } from "./components/administracion/mapa/mapa.component";
import { VerificacionEmailComponent } from "./components/administracion/registrar-usuario/verificacion-email/verificacion-email.component";
import { SeleccionarTipoComponent } from "./components/administracion/registrar-usuario/seleccionar-tipo/seleccionar-tipo.component";
import { SeleccionarNombreComponent } from "./components/administracion/registrar-usuario/seleccionar-nombre/seleccionar-nombre.component";
import { SeleccionarNacimientoComponent } from "./components/administracion/registrar-usuario/seleccionar-nacimiento/seleccionar-nacimiento.component";

import {
  OWL_DATE_TIME_LOCALE,
  OwlDateTimeIntl,
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
  OWL_DATE_TIME_FORMATS,
  DateTimeAdapter
} from "ng-pick-datetime";
import { SpanishIntl } from "./config/datepicker.i18n";
import { MomentDateTimeAdapter }  from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';
import { CertificarTelefonoComponent } from "./components/administracion/registrar-usuario/certificar-telefono/certificar-telefono.component";
import { SeleccionarGeneroComponent } from "./components/administracion/registrar-usuario/seleccionar-genero/seleccionar-genero.component";
import { IngresarContraseniaComponent } from "./components/administracion/registrar-usuario/ingresar-contrasenia/ingresar-contrasenia.component";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DefinirTrayectoComponent } from "./components/administracion/publicar-viajes/definir-trayecto/definir-trayecto.component";
import { DefinirAportacionComponent } from "./components/administracion/publicar-viajes/definir-aportacion/definir-aportacion.component";
import { PerfilUsuarioComponent } from "./components/administracion/cuenta-usuario/perfil-usuario/perfil-usuario.component";
import { InfoCochesComponent } from "./components/administracion/cuenta-usuario/info-coches/info-coches.component";
import { ReservaAutomaticaComponent } from "./components/administracion/publicar-viajes/reserva-automatica/reserva-automatica.component";
import { CerficacionesComponent } from "./components/administracion/cuenta-usuario/cerficaciones/cerficaciones.component";
import { PublicarConfirmacionComponent } from "./components/administracion/publicar-viajes/publicar-confirmacion/publicar-confirmacion.component";
import { DefinirFiltrosComponent } from './components/administracion/buscar-viajes/definir-filtros/definir-filtros.component';
import { PreferenciasComponent } from './components/administracion/cuenta-usuario/preferencias/preferencias.component';
import { InfoPersonalComponent } from './components/administracion/cuenta-usuario/info-personal/info-personal.component';
import { ListadoViajesComponent } from './components/administracion/buscar-viajes/listado-viajes/listado-viajes.component';
import { DetallesViajeComponent } from './components/administracion/buscar-viajes/detalles-viaje/detalles-viaje.component';
import { PerfilConductorComponent } from './components/administracion/buscar-viajes/perfil-conductor/perfil-conductor.component';
import { ReservarViajeComponent } from './components/administracion/buscar-viajes/reservar-viaje/reservar-viaje.component';
import { SolicitudesComponent } from './components/administracion/cuenta-usuario/solicitudes/solicitudes.component';
import { MisviajesComponent } from './components/administracion/cuenta-usuario/misviajes/misviajes.component';

export const MY_CUSTOM_FORMATS = {
  parseInput: "DD/MM/YYYY",
  fullPickerInput: "DD/MM/YYYY hh:mm a",
  datePickerInput: "DD/MM/YYYY",
  timePickerInput: "HH:mm",
  monthYearLabel: "MMM-YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM-YYYY"
};

const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  overlayColor: "white",
  bgsColor: "#FFFFFF",
  bgsOpacity: 1,
  fgsColor: "#009b72",
  bgsPosition: POSITION.bottomCenter,
  bgsSize: 40,
  fgsType: SPINNER.pulse,
  hasProgressBar: false,
  textColor: "black",
  blur: 5
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    MapaComponent,
    VerificacionEmailComponent,
    SeleccionarTipoComponent,
    SeleccionarNombreComponent,
    SeleccionarNacimientoComponent,
    CertificarTelefonoComponent,
    SeleccionarGeneroComponent,
    IngresarContraseniaComponent,
    DefinirTrayectoComponent,
    DefinirAportacionComponent,
    PerfilUsuarioComponent,
    InfoCochesComponent,
    ReservaAutomaticaComponent,
    CerficacionesComponent,
    PublicarConfirmacionComponent,
    DefinirFiltrosComponent,
    PreferenciasComponent,
    InfoPersonalComponent,
    ListadoViajesComponent,
    DetallesViajeComponent,
    PerfilConductorComponent,
    ReservarViajeComponent,
    SolicitudesComponent,
    MisviajesComponent
  ], 
  imports: [
    FacebookModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig)
  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {
      provide: "RedirectGuard",
      useValue: (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
        window.location.href = "pagina-404.html";
        return false;
      }
    },
    { provide: LOCALE_ID, useValue: "es" },
    { provide: OWL_DATE_TIME_LOCALE, useValue: "es" },
    { provide: OwlDateTimeIntl, useClass: SpanishIntl },
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE]
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
 
export class AppModule {}
