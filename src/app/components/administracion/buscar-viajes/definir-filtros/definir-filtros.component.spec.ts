import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinirFiltrosComponent } from './definir-filtros.component';

describe('DefinirFiltrosComponent', () => {
  let component: DefinirFiltrosComponent;
  let fixture: ComponentFixture<DefinirFiltrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinirFiltrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinirFiltrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
