import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
import { NgxUiLoaderService } from "ngx-ui-loader"; // Import NgxUiLoaderService
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { ToastrService } from "ngx-toastr";
@Component({
  selector: 'app-definir-filtros',
  templateUrl: './definir-filtros.component.html',
  styleUrls: ['./definir-filtros.component.scss']
})
export class DefinirFiltrosComponent implements OnInit {
  hora_actual: any;
  plazas: number = 1;
  lat = -25.311951;
  lng = -57.587114;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  string_plaza: string = "pasajero";
  autocomplete;
  map: google.maps.Map;
  autocomplete1;
  inicio_isvalid: boolean = false;
  fin_isvalid: boolean = false;
  coordenadas_inicio: any = [];
  coordenadas_fin: any = [];
  lugar_inicio: any;
  lugar_destino: any;
  horaSalida = new Date();
  fechaSalida = new Date();
  obj_filtro: any = {};

  section_principal: boolean = true;
  section_fecha: boolean = false;
  section_plazas: boolean = false;
  section_listado: boolean = false;
  section_visualizacion: boolean = false;
  section_perfil: boolean = false;
  section_mapa: boolean = false;
  section_reserva: boolean = false;

  ingresoFechaForm: FormGroup;
  @Input() name: string = '';
  @Input() model: any;
  @Input() placeholder: string = 'DD/MM/YYYY';
  @Input() placeholderHora: string = 'HH:MM';
  @Input() type: string = 'calendar';
  @Input() type2: string = 'timer';
  @Output() dateChange = new EventEmitter();
  mapOptions: google.maps.MapOptions = {
    disableDefaultUI: false,
    mapTypeControl: false,
    streetViewControl: false,
    scaleControl: true,
    zoomControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: this.coordinates,
    zoom: 13,
    styles: [
      {
        featureType: "landscape",
        stylers: [
          {
            hue: "#FFBB00"
          },
          {
            saturation: 23.400000000000006
          },
          {
            lightness: 17.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.highway",
        stylers: [
          {
            hue: "#FFC200"
          },
          {
            saturation: -61.8
          },
          {
            lightness: 45.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.arterial",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 51.19999999999999
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.local",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 52
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "water",
        stylers: [
          {
            hue: "#0078FF"
          },
          {
            saturation: -13.200000000000003
          },
          {
            lightness: 2.4000000000000057
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "poi",
        stylers: [
          {
            hue: "#009b72"
          },
          // {
          //   saturation: -1.0989010989011234
          // },
          // {
          //   lightness: 11.200000000000017
          // },
          // {
          //   gamma: 1
          // }
        ]
      }
    ]
  };


  constructor(private toastr: ToastrService, private router: Router, public dateService: DateService, private ngxService: NgxUiLoaderService, private fb: FormBuilder) {

  }


  changeValue(value) {
    this.plazas += value;
  }

  ngOnInit() {

    if (sessionStorage.getItem("obj_filtro")) {
      this.obj_filtro = JSON.parse(sessionStorage.getItem("obj_filtro"));
    }
    if (this.obj_filtro["fechaViaje"] && this.obj_filtro["horaViaje"]) {
      console.log("tiene fecha ")
      this.ingresoFechaForm = this.fb.group({
        fechaSalida: [{ value: this.obj_filtro["fechaViaje"] }],
        horaSalida: [{ value: this.obj_filtro["horaViaje"] }]
      });
    } else {
      this.ingresoFechaForm = this.fb.group({
        fechaSalida: [{ value: "" }],
        horaSalida: [{ value: "" }]
      });
    }
    this.hora_actual = this.dateService.getOnlyTime(new Date(Date.now()))
    this.autocompleteInitializer();
  }
  get f() { return this.ingresoFechaForm.controls; }
  autocompleteInitializer() {
    var inputSalida = document.getElementById('pac-input');
    var inputLlegada = document.getElementById('pac-input1');
    //Init autocomplete
    this.autocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>inputSalida);
    this.autocomplete1 = new google.maps.places.Autocomplete(<HTMLInputElement>inputLlegada);

    google.maps.event.addListener(this.autocomplete, 'place_changed', (function (coordenadas_inicio, obj_filtro) {

      return function () {
        var place = this.getPlace();

        if (place.geometry) {
          var marker = new google.maps.Marker({
            position: place.geometry.location
          })
          //coordenadas_fin.push({ "lat": marker.getPosition().lat().toFixed(2), "lng": marker.getPosition().lng().toFixed(2) });
          coordenadas_inicio.push({ "lat": marker.getPosition().lat(), "lng": marker.getPosition().lng() });
          obj_filtro["coordenadas_inicio"] = coordenadas_inicio[0];
        }
      };
    })(this.coordenadas_inicio, this.obj_filtro)); 

    google.maps.event.addListener(this.autocomplete1, 'place_changed', (function (coordenadas_fin, obj_filtro) {

      return function () {
        var place = this.getPlace();

        if (place.geometry) {
          var marker = new google.maps.Marker({
            position: place.geometry.location
          })
          //coordenadas_fin.push({ "lat": marker.getPosition().lat().toFixed(2), "lng": marker.getPosition().lng().toFixed(2) });
          coordenadas_fin.push({ "lat": marker.getPosition().lat(), "lng": marker.getPosition().lng() });
          obj_filtro["coordenadas_fin"] = coordenadas_fin[0];
        }
      };
    })(this.coordenadas_fin, this.obj_filtro));

  }

  seleccionarFecha() {
    var inputSalida_valid = ((<any>document.getElementById('pac-input')).value != "");
    var inputLlegada_valid = ((<any>document.getElementById('pac-input1')).value != "");

    if (inputSalida_valid && inputLlegada_valid) {
      this.obj_filtro["lugarSalida"] = (<any>document.getElementById('pac-input')).value;
      this.obj_filtro["lugarLlegada"] = (<any>document.getElementById('pac-input1')).value;

      if (this.obj_filtro["fechaViaje"] && this.obj_filtro["horaViaje"]) {
        this.fechaSalida.setDate((this.obj_filtro["fechaViaje"].substr(0, 2)));
        this.fechaSalida.setMonth(this.obj_filtro["fechaViaje"].substr(3, 2)-1);
        this.fechaSalida.setFullYear(this.obj_filtro["fechaViaje"].substr(6, 4));

        this.horaSalida.setHours(this.obj_filtro["horaViaje"].substr(0, 2));
        this.horaSalida.setMinutes(this.obj_filtro["horaViaje"].substr(3, 2));

        this.ingresoFechaForm = this.fb.group({
          fechaSalida: new FormControl(this.fechaSalida),
          horaSalida: new FormControl(this.horaSalida)
        });
      }

      this.section_principal = false;
      this.section_fecha = true;
      window.scroll(0, 0);
    }
  }

  seleccionarCantPlazas() {
    var inputSalida_valid = ((<any>document.getElementById('id_fechaIda')).value != "");
    var inputLlegada_valid = ((<any>document.getElementById('id_horaIda')).value != "");
    if (inputSalida_valid && inputLlegada_valid) {
      this.obj_filtro["fechaViaje"] = (<any>document.getElementById('id_fechaIda')).value;
      this.obj_filtro["horaViaje"] = (<any>document.getElementById('id_horaIda')).value + ":00";
      if (this.obj_filtro["asientosDisponibles"]) {
        this.plazas = this.obj_filtro["asientosDisponibles"];
      }
      this.section_fecha = false;
      this.section_plazas = true;
      //var section_fecha = document.getElementById("id_section_fecha").style.display = "none";
      //var section_plazas = document.getElementById("id_section_plazas").style.display = "block";
      window.scroll(0, 0);
    }
  }

  seleccionarPlazas() {
    //var section_plazas = document.getElementById("id_section_plazas").style.display = "none";
    this.obj_filtro["asientosDisponibles"] = this.plazas;
    sessionStorage.setItem("obj_filtro", JSON.stringify(this.obj_filtro));
    this.section_plazas = false;
    this.router.navigate(["/buscar/viajes"]);
  }



  redirectFormFromPrincipal() {
    //var section_principal = document.getElementById("id_section_principal").style.display = "block";
    //var section_fecha = document.getElementById("id_section_fecha").style.display = "none";
    this.section_principal = true;
    this.section_fecha = false;
    window.scroll(0, 0);
  }
  redirectFormFromFecha() {
    //var section_fecha = document.getElementById("id_section_fecha").style.display = "block";
    //var section_plazas = document.getElementById("id_section_plazas").style.display = "none";
    this.section_fecha = true;
    this.section_plazas = false;
    window.scroll(0, 0);
  }

  verViaje() {
    //var section_principal = document.getElementById("id_section_visualizacion").style.display = "block";
    //var section_listado = document.getElementById("id_section_listado").style.display = "none";
    this.section_visualizacion = true;
    this.section_listado = false;
    window.scroll(0, 0);
  }

  verPerfilConductor() {
    this.section_visualizacion = false;
    this.section_perfil = true;
    window.scroll(0, 0);
  }

  volverListado() {
    //var section_principal = document.getElementById("id_section_visualizacion").style.display = "none";
    //var section_listado = document.getElementById("id_section_listado").style.display = "block";
    this.section_visualizacion = false;
    this.section_listado = true;
    window.scroll(0, 0);
  }

  volverVisualizacion() {
    this.section_visualizacion = true;
    this.section_perfil = false;
    window.scroll(0, 0);
  }

  editarFiltro() {
    this.section_principal = true;
    this.section_listado = false;
    window.scroll(0, 0);
  }



}
