import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesViajeComponent } from './detalles-viaje.component';

describe('DetallesViajeComponent', () => {
  let component: DetallesViajeComponent;
  let fixture: ComponentFixture<DetallesViajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallesViajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
