import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { DynamicscriptloaderService } from 'src/app/services/dynamicscriptloader.service';
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { DateService } from 'src/app/services/date.service';

@Component({
  selector: 'app-detalles-viaje',
  templateUrl: './detalles-viaje.component.html',
  styleUrls: ['./detalles-viaje.component.scss']
})
export class DetallesViajeComponent implements OnInit {
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;
  section_mapa: boolean = false;
  section_visualizacion: boolean = true;
  lat1 = -25.313833;
  lng1 = -57.595007;
  lat2 = -25.2868614;
  lng2 = -57.5887536;
  coordinates = new google.maps.LatLng(this.lat1, this.lng1);
  map: google.maps.Map;
  viaje: any = {};
  detalleViaje: any = {};
  detViajeBuscado: any = {};
  fechaSalida: String;
  mapOptions: google.maps.MapOptions = {
    disableDefaultUI: false,
    mapTypeControl: false,
    streetViewControl: false,
    scaleControl: true,
    zoomControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: this.coordinates,
    zoom: 13,
    styles: [
      {
        featureType: "landscape",
        stylers: [
          {
            hue: "#FFBB00"
          },
          {
            saturation: 23.400000000000006
          },
          {
            lightness: 17.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.highway",
        stylers: [
          {
            hue: "#FFC200"
          },
          {
            saturation: -61.8
          },
          {
            lightness: 45.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.arterial",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 51.19999999999999
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.local",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 52
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "water",
        stylers: [
          {
            hue: "#0078FF"
          },
          {
            saturation: -13.200000000000003
          },
          {
            lightness: 2.4000000000000057
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "poi",
        stylers: [
          {
            hue: "#009b72"
          },
          // {
          //   saturation: -1.0989010989011234
          // },
          // {
          //   lightness: 11.200000000000017
          // },
          // {
          //   gamma: 1
          // }
        ]
      }
    ]
  };
  constructor(private dateService: DateService, private datePipe: DatePipe, private toastr: ToastrService, private ngxService: NgxUiLoaderService, private router: Router, private route: ActivatedRoute, private buscarService: BuscarViajesService) { }

  ngOnInit() {
    //this.getDetallesViaje(this.route.snapshot.paramMap.get("id"));
    this.detViajeBuscado = JSON.parse(sessionStorage.getItem("detalleViaje"));
    //Se obtiene fecha del viaje
    this.fechaSalida = this.getFechaViaje();
  }

  getFechaViaje() {
    if (this.detViajeBuscado.viaje.fechaViaje && this.detViajeBuscado.viaje.horaViaje) {
      var fechaViaje: any = this.detViajeBuscado.viaje.fechaViaje;
      var horaViaje: any = this.detViajeBuscado.viaje.horaViaje
      var fechaSalidaAux = new Date(fechaViaje.substr(6, 4), fechaViaje.substr(3, 2) - 1, fechaViaje.substr(0, 2), horaViaje.substr(0, 2), horaViaje.substr(3, 2));
      return this.datePipe.transform(fechaSalidaAux, "EEEE, d MMMM y, HH:mm");
    }
  }

  formatTime(hours) {
    if (hours) {
      var stringTime = hours.split(":")
      var date = new Date();
      date.setHours(stringTime[0])
      date.setMinutes(stringTime[1])
      return this.dateService.getOnlyTime(date);
    }
  }
  formatDistancia(distancia) {
    if (distancia != "" && distancia && distancia>0) {
      return parseFloat(distancia).toFixed(2);
    }else if(distancia==0){
      return 0;
    }
  }
  getDetallesViaje(id) {
    this.ngxService.start();
    this.buscarService.getDataViaje(id).subscribe(response => {
      this.ngxService.stop();
      if (response) {
        if (response.estado == 0) {
          this.viaje = response.data;
          if (this.viaje.viaje) {
            this.detalleViaje = this.viaje.viaje;
          }
          var fechaViaje: any = this.viaje.viaje.fechaViaje;
          var horaViaje: any = this.viaje.viaje.horaViaje
          var fechaSalidaAux = new Date(fechaViaje.substr(6, 4), fechaViaje.substr(3, 2) - 1, fechaViaje.substr(0, 2), horaViaje.substr(0, 2), horaViaje.substr(3, 2));
          this.fechaSalida = this.datePipe.transform(fechaSalidaAux, "EEEE, d MMMM y, HH:mm");
        } else {
          this.toastr.info(response.mensaje);
        }
      }
    });
  }
  verMapa(id) {
    this.section_mapa = true;
    this.section_visualizacion = false;

    var iconOrigen = {
      url: "assets/img/icons/theme/map/marker-3.png", // url
      scaledSize: new google.maps.Size(40, 40), // scaled size
    };

    var iconDestino = {
      url: "assets/img/icons/theme/map/marker-3.png", // url
      scaledSize: new google.maps.Size(40, 40), // scaled size
    };

    setTimeout(() => {
      this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
      var viaje = this.detViajeBuscado.viaje;
      var lugaresDePaso = this.detViajeBuscado.ciudadesPaso;
      var coorOrigen = new google.maps.LatLng(viaje.latitudOrigen, viaje.longitudOrigen);
      var coorDestino = new google.maps.LatLng(viaje.latitudDestino, viaje.longitudDestino);

      var infowindowOrigen = new google.maps.InfoWindow();
      var content = "<h6 style='margin-bottom:10px;'>" + viaje.lugarOrigen + "</h6>" +
        "<p class='mb-2'> <strong>Fecha salida: </strong>" + viaje.fechaViaje + "</p>" +
        "<p class='mb-2'> <strong>Hora salida: </strong>" + this.formatTime(viaje.horaViaje) + "</p>" +
        "<p class='mb-2'> <strong>Distancia al punto de salida solicitado: </strong>" + this.formatDistancia(this.detViajeBuscado.distanciaOrigen) + " km </p>";

      var markerOrigen = new google.maps.Marker({
        position: coorOrigen,
        map: this.map,
        icon: iconOrigen
      });


      google.maps.event.addListener(markerOrigen, 'click', (function (marker, content, infowindow) {
        return function () {
          infowindow.setContent(content);
          infowindow.open(this.map, marker);
        };
      })(markerOrigen, content, infowindowOrigen));

      var infowindowDestino = new google.maps.InfoWindow();
      var contentDestino = "<h6 style='margin-bottom:10px;'>" + viaje.lugarDestino + "</h6>" +
        "<p class='mb-2'> <strong>Fecha salida: </strong>" + viaje.fechaViaje + "</p>" +
        "<p class='mb-2'> <strong>Hora llegada: </strong>" + this.formatTime(viaje.horaLlegada) + "</p>" +
        "<p class='mb-2'> <strong>Distancia al punto de llegada solicitado: </strong>" + this.formatDistancia(this.detViajeBuscado.distanciaDestino) + " km </p>";

      var markerDestino = new google.maps.Marker({
        position: coorDestino,
        map: this.map,
        icon: iconDestino
      });

      google.maps.event.addListener(markerDestino, 'click', (function (marker, content, infowindow) {
        return function () {
          infowindow.setContent(content);
          infowindow.open(this.map, marker);
        };
      })(markerDestino, contentDestino, infowindowDestino));

      markerOrigen.setMap(this.map);
      markerDestino.setMap(this.map);
      console.log(lugaresDePaso)
      if (lugaresDePaso.length > 0) {
        lugaresDePaso.forEach(element => {
          var coordenadas = new google.maps.LatLng(element.latitud, element.longitud);
          var markerPaso = new google.maps.Marker({
            position: coordenadas,
            map: this.map,
            icon: iconDestino
          });
          markerPaso.setMap(this.map);
        });
      }

      if (id == 0) {
        this.map.setCenter({ lat: markerOrigen.getPosition().lat(), lng: markerOrigen.getPosition().lng() })
        infowindowOrigen.setContent(content);
        infowindowOrigen.open(this.map, markerOrigen);
      } else {
        infowindowDestino.setContent(contentDestino);
        infowindowDestino.open(this.map, markerDestino);
        this.map.setCenter({ lat: markerDestino.getPosition().lat(), lng: markerDestino.getPosition().lng() })
      }
      this.map.setZoom(15)
    }, 150);
  }

  salirMapa() {
    this.section_mapa = false;
    this.section_visualizacion = true;
  }

  volverListado() {
    this.router.navigate(["/buscar/viajes"]);
    window.scroll(0, 0);
  }
  verPerfilConductor() {
    this.router.navigate(["/buscar/viajes/" + this.detViajeBuscado.viaje.id + "/conductor"]);
    window.scroll(0, 0);
  }
  reservarViaje() {
    this.router.navigate(["/buscar/viajes/" + this.detViajeBuscado.viaje.id + "/reservar"]);
    window.scroll(0, 0);
  }
}
