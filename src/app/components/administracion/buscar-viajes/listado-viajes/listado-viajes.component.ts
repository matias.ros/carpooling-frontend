import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
import { DatePipe } from '@angular/common'
import { ToastrService } from 'ngx-toastr';
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-listado-viajes',
  templateUrl: './listado-viajes.component.html',
  styleUrls: ['./listado-viajes.component.scss']
})
export class ListadoViajesComponent implements OnInit {

  section_listado: boolean = true;

  section_perfil: boolean = false;

  section_reserva: boolean = false;
  horaViaje: any = new Date();
  horaLlegada: any = new Date();
  obj_filtro: any = {};
  fechaSalida: String;
  viajes: any = [];
  radioDistancia:any = 25;
  abrirFiltroResponsive:boolean=false;
  constructor(private ngxService: NgxUiLoaderService, private buscarService: BuscarViajesService, private toastr: ToastrService, private router: Router, private dateService: DateService, private datePipe: DatePipe) { }

  ngOnInit() {
    if (sessionStorage.getItem("obj_filtro")) {
      this.obj_filtro = JSON.parse(sessionStorage.getItem("obj_filtro"));
      console.log()
      var fechaSalidaAux = new Date(this.obj_filtro.fechaViaje.substr(6, 4), this.obj_filtro.fechaViaje.substr(3, 2) - 1, this.obj_filtro.fechaViaje.substr(0, 2), this.obj_filtro.horaViaje.substr(0, 2), this.obj_filtro.horaViaje.substr(3, 2));
      console.log(fechaSalidaAux)
      this.fechaSalida = this.datePipe.transform(fechaSalidaAux, "E, d MMMM, y, HH:mm");
    }
    this.buscarViajes(0);
  }
  cambiarRadio($event){
    console.log($event)
  }
  buscarViajes(id) {
    this.ngxService.start();
    var data = {
      "fechaViaje": this.obj_filtro["fechaViaje"],
      "horaViaje": this.obj_filtro["horaViaje"],
      "asientosDisponibles": this.obj_filtro["asientosDisponibles"],
      "latOrigen": this.obj_filtro["coordenadas_inicio"]["lat"],
      "lonOrigen": this.obj_filtro["coordenadas_inicio"]["lng"],
      "latDestino": this.obj_filtro["coordenadas_fin"]["lat"],
      "lonDestino": this.obj_filtro["coordenadas_fin"]["lng"],
      "rad": (<any>document.getElementById("id_radio")).value,
      "soloMujeres": (<any>document.getElementById("check-5")).checked,
      "fumar": (<any>document.getElementById("check-3")).checked,
      "bebes": (<any>document.getElementById("check-1")).checked,
      "mascota": (<any>document.getElementById("check-2")).checked,
      "idTamanhoEquipaje": this.obj_filtro["idTamanhoEquipaje"],
      "maximoDosAtras": (<any>document.getElementById("check-4")).checked

    }
    this.buscarService.buscarViajes(data).subscribe(response => {
      this.ngxService.stop();
      if (response) {
        if (response.estado == 0) {
          this.viajes = response.data;
        } else {
          this.toastr.info(response.mensaje);
        }
      }
    });
    if(id==1){
      this.abrirFiltroResponsive = false;
    }
  }

  filtrarResponsive(id){
    //abrir
    if(id==0){
      this.abrirFiltroResponsive = true;
    }else if(id==1){
      this.abrirFiltroResponsive = false;
    }
  }
  formatTime(hours) {
    if (hours) {
      var stringTime = hours.split(":")
      var date = new Date();
      date.setHours(stringTime[0])
      date.setMinutes(stringTime[1])
      return this.dateService.getOnlyTime(date);
    }
  }
  verViaje(id) {
    sessionStorage.removeItem("detalleViaje");
    var detalles = {};
    detalles = this.viajes.find(item => item.viaje.id === id);
    sessionStorage.setItem("detalleViaje", JSON.stringify(detalles));
    this.router.navigate(["/buscar/viajes/" + id + "/detalles"]);
    window.scroll(0, 0);
  }
  sectionOpacity($event) {
    if(document.getElementById("section-opacity").classList.contains("section-opacity-class")){
    var section = document.getElementById("section-opacity").classList.remove("section-opacity-class");
    var btn = document.getElementById("btn-sidemenu");
    }else{
      var section = document.getElementById("section-opacity").classList.add("section-opacity-class");
    }
  }
  selectSize(size) {
    var sizes = (<any>document.getElementsByClassName("sizes"))
    if (size == 1) {
      if (!sizes[0].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
      }
    } else if (size == 2) {
      if (!sizes[1].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
      } else if (!sizes[2].classList.contains("selected-size")) {
        sizes[1].classList.remove("selected-size");
      }
    } else if (size == 3) {
      if (!sizes[2].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
        sizes[2].classList.add("selected-size");
      } else {
        sizes[2].classList.remove("selected-size");
      }
    }
    this.obj_filtro["idTamanhoEquipaje"] = size;
  }
  editarFiltro() {
    this.router.navigate(["/buscar"])
    window.scroll(0, 0);
  }

  volverVisualizacion() {
    //this.section_visualizacion = true;
    this.section_perfil = false;
    window.scroll(0, 0);
  }



  reservarViaje() {
    //this.section_visualizacion = false;
    this.section_reserva = true;
    window.scroll(0, 0);
  }

}
