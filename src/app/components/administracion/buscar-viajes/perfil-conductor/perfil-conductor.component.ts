import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DateService } from 'src/app/services/date.service';
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-perfil-conductor',
  templateUrl: './perfil-conductor.component.html',
  styleUrls: ['./perfil-conductor.component.scss']
})
export class PerfilConductorComponent implements OnInit {

  constructor(private datePipe: DatePipe,private toastr: ToastrService,private ngxService :NgxUiLoaderService,private buscarService:BuscarViajesService,private router: Router, private dateService: DateService) { }
  detalleViaje: any = {};
  preferencias: any = {};
  misViajes: any = [];
  edad: any;
  ngOnInit() {
    this.detalleViaje = JSON.parse(sessionStorage.getItem("detalleViaje"));
    this.preferencias = this.detalleViaje.preferencias[0];
    console.log(this.detalleViaje.miembro.fechaNacimiento)
    this.edad = this.getAge(this.detalleViaje.miembro.fechaNacimiento)
    this.listarTodosMisViajes();
  }
  volverVisualizacion() {
    this.router.navigate(["/buscar/viajes/" + this.detalleViaje.viaje.id + "/detalles"]);
  }
  getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
  getFechaViaje() {
    if (this.detalleViaje.miembro.fechaCreacion) {
      var fechaCreacion: any = this.detalleViaje.miembro.fechaCreacion
      var fechaSalidaAux = new Date(fechaCreacion.substr(6, 4), fechaCreacion.substr(3, 2) - 1, fechaCreacion.substr(0, 2));
      return this.datePipe.transform(fechaSalidaAux, " 'Usuario desde el ' d 'de' MMMM 'del' y");
    }
  }
  listarTodosMisViajes() { 
    this.ngxService.start();
    var id = this.detalleViaje.miembro.id;
    this.buscarService.listarViajesConductor(id).subscribe(response => {
      this.ngxService.stop();
      if (response) {
        if (response.estado == 0) {
          this.misViajes = response.data;
        } else {
          this.toastr.info("Mis viajes: "+response.mensaje);
        }
      }
    });
  }

}
