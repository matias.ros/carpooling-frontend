import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
import { DatePipe } from '@angular/common';
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reservar-viaje',
  templateUrl: './reservar-viaje.component.html',
  styleUrls: ['./reservar-viaje.component.scss']
})
export class ReservarViajeComponent implements OnInit {

  constructor(private toastr: ToastrService, private ngxService: NgxUiLoaderService, private buscarService: BuscarViajesService, private dateService: DateService, private datePipe: DatePipe, private router: Router) { }
  detViajeBuscado: any = {};
  obj_filtro:any = {};
  currentUser:any = {};
  reservadoActualmente:any = false;
  hombreNoPuedeReservar:any = false;
  fechaSalida: any = "";
  solicitudes: any = [];
  preferencias:any = {};
  asientosDisponibles: any = [];

  ngOnInit() {
    this.detViajeBuscado = JSON.parse(sessionStorage.getItem("detalleViaje"));
    this.obj_filtro = JSON.parse(sessionStorage.getItem("obj_filtro"));
    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    this.preferencias = this.detViajeBuscado.preferencias[0];
    this.getReservasActuales();
    this.fechaSalida = this.getFechaViaje();
    var sexoPasajero = this.currentUser.sexo == "MASCULINO";
    var soloMujeres = this.detViajeBuscado.viaje.soloMujeres;
    this.hombreNoPuedeReservar = sexoPasajero && soloMujeres;  
  }

  getFechaViaje() {
    if (this.detViajeBuscado.viaje.fechaViaje && this.detViajeBuscado.viaje.horaViaje) {
      var fechaViaje: any = this.detViajeBuscado.viaje.fechaViaje;
      var horaViaje: any = this.detViajeBuscado.viaje.horaViaje
      var fechaSalidaAux = new Date(fechaViaje.substr(6, 4), fechaViaje.substr(3, 2) - 1, fechaViaje.substr(0, 2), horaViaje.substr(0, 2), horaViaje.substr(3, 2));
      return this.datePipe.transform(fechaSalidaAux, "EEEE, d MMMM y, HH:mm");
    }
  }

  getReservasActuales() {
    this.ngxService.start();
    this.buscarService.listarSolicitudes(this.detViajeBuscado.viaje.id).subscribe(response => {
      this.ngxService.stop();
      if (response) {
        if (response.estado == 0) {
          this.solicitudes = response.data;
          if(this.solicitudes){
            var soliAprobadasOrechazadas = 0;
            this.solicitudes.forEach(element => {
              if(element.solicitud.idEstadoSolicitud != 3){
                soliAprobadasOrechazadas = soliAprobadasOrechazadas +1;
                if(element.pasajero.id == this.currentUser.id){
                  this.reservadoActualmente = true;
                }
              }
            });
            this.asientosDisponibles.length = this.detViajeBuscado.viaje.cantidadAsientosOfrecidos - soliAprobadasOrechazadas;
          }
        } else {
          this.toastr.info(response.mensaje);
        }
      }
    });
  }
  
  reservarViaje(){
    this.ngxService.start();
    var data = {
      "idViaje": this.detViajeBuscado.viaje.id,
      "cantidadPasajeros":  this.obj_filtro.asientosDisponibles
    }
   
    this.buscarService.reservarViaje(data).subscribe(response => {
      this.ngxService.stop();
      if (response) {
        if (response.estado == 0) {
          if(this.detViajeBuscado.viaje.aceptacionAutomatica){
            this.toastr.info("¡Lugar reservado con éxito!");
          }else{
            this.toastr.info("¡El conductor se comunicará con usted para confirmar la reserva!");
          }
          this.getReservasActuales();
          this.asientosDisponibles.length = this.asientosDisponibles.length - this.obj_filtro.asientosDisponibles;
          this.obj_filtro.asientosDisponibles = this.asientosDisponibles.length;
          sessionStorage.setItem("obj_filtro",JSON.stringify(this.obj_filtro));
        } else {
          this.toastr.info(response.mensaje);
        }
      }
    });
  }

  volverDetalles() {
    this.router.navigate(["/buscar/viajes/" + this.detViajeBuscado.viaje.id + "/detalles"]);
    window.scroll(0, 0);
  }

}
