import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CerficacionesComponent } from './cerficaciones.component';

describe('CerficacionesComponent', () => {
  let component: CerficacionesComponent;
  let fixture: ComponentFixture<CerficacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CerficacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CerficacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
