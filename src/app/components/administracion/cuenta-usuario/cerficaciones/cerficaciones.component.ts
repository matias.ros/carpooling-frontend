import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { OauthService } from "src/app/services/oauth.service";
import { ToastrService } from "ngx-toastr";
import { FacebookService, LoginResponse, LoginOptions } from "ngx-facebook";
import { RegistroService } from "src/app/services/registro.service";

@Component({
  selector: "app-cerficaciones",
  templateUrl: "./cerficaciones.component.html",
  styleUrls: ["./cerficaciones.component.scss"]
})
export class CerficacionesComponent implements OnInit {
  aux: any = {};
  certificaciones: any;
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public registroService: RegistroService,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService,
    public oauthService: OauthService,
    private fb: FacebookService
  ) {}

  getCertificaciones() {
    this.ngxService.start();
    this.registroService.verCertificaciones().subscribe(response => {
      if (response.estado == 0) {
        this.aux = response.data;
        console.log(this.aux);
        this.ngxService.stop();
      } else {
        this.ngxService.stop();
        this.toastr.error(response.mensaje);
      }
    });
  }

  sendCorreo() {
    this.registroService.enviarCorreo().subscribe(response => {
      if (response.estado == 0) {
        this.toastr.info(
          "Reenviamos un correo de verificación a tu e-mail",
          ""
        );
        console.log(this.aux);
      } else {
        this.toastr.error(response.mensaje, "Atención");
      }
    });
  }

  fbVerificacion(data) {
    var param = {
      token: data
    };
    this.registroService.certificarFacebook(param).subscribe(response => {
      //window.location.reload();
      this.getCertificaciones();
    });
  }

  loginFB() {
    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: "user_birthday,user_photos,email,public_profile",
      auth_type: "rerequest"
    };
    this.fb
      .login(loginOptions)
      .then((response: LoginResponse) => {
        if (response.authResponse && response.status === "connected") {
          this.fbVerificacion(response.authResponse.accessToken);
        } else {
          // this.showAlert = true;
          // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
        }
      })
      .catch((error: any) => {
        // this.showAlert = true;
        // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
      });
  }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
    this.getCertificaciones();
    if (this.router.url.includes("/certificaciones")) {
      console.log("incluye /certificaciones");
      var tabelement = document.getElementById("tab-certificaciones");
      tabelement.classList.add("active");
      var divelement = document.getElementById("certificaciones");
      divelement.classList.add("active", "show");
    }
  }
}
