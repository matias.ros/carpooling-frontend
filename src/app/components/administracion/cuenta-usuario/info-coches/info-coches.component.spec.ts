import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoCochesComponent } from './info-coches.component';

describe('InfoCochesComponent', () => {
  let component: InfoCochesComponent;
  let fixture: ComponentFixture<InfoCochesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoCochesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoCochesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
