import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ToastrService } from "ngx-toastr";
import { CocheService } from "src/app/services/coche.service";
import { Location } from "@angular/common";
import {
  listaMarcas,
  marcasFrecuentes,
  colores
} from "../../../../config/constants";

@Component({
  selector: "app-info-coches",
  templateUrl: "./info-coches.component.html",
  styleUrls: ["./info-coches.component.scss"]
})
export class InfoCochesComponent implements OnInit {
  marcas: any;
  marcasFrec = marcasFrecuentes;
  listaColores = colores;
  datosCoche: any = {};
  uploadFoto: any = {};
  nuevoCoche = false;
  showFrecuentes = true;
  filtroMarca: any = "";
  nombreModelo: any = "";
  filteredItems: any = [];
  cochesUsuario: any = [];
  seleccionMarca = false;
  seleccionModelo = false;
  seleccionColor = false;
  confirmacionExito = false;
  fotoCoche = false;
  chapaAnho = false;
  nombreColor: any;
  numeroChapa: "";
  anho: "";
  mensajeChapa = "";

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public cocheService: CocheService,
    private location: Location,
    private ngxService: NgxUiLoaderService,
    private toastr: ToastrService
  ) {}

  /* COCHE */

  getCoches() {
    this.ngxService.start();
    this.cocheService.listarCoches().subscribe(response => {
      if (response.estado == 0) {
        this.ngxService.stop();
        this.cochesUsuario = response.data;
        console.log(response);
      } else {
        this.ngxService.stop();
      }
    });
  }

  assignCopy() {
    this.filteredItems = Object.assign([], this.marcas);
  }

  filterItem(value) {
    this.showFrecuentes = false;
    if (!value) {
      this.assignCopy();
    }
    this.filteredItems = Object.assign([], this.marcas).filter(
      item => item.descripcion.toLowerCase().indexOf(value.toLowerCase()) > -1
    );
  }

  newCoche() {
    this.listarMarcas();
    this.nuevoCoche = true;
    this.seleccionMarca = true;
  }

  cancelar() {
    this.datosCoche = {};
    this.nuevoCoche = false;
    this.filtroMarca = "";
    this.nombreModelo = "";
    this.numeroChapa = "";
    this.anho = "";
    this.nombreColor = "";
    this.filteredItems = [];
    this.seleccionMarca = false;
    this.seleccionModelo = false;
    this.seleccionColor = false;
    this.confirmacionExito = false;
    this.fotoCoche = false;
    this.chapaAnho = false;
  }

  setMarca(item) {
    console.log(item);
    this.datosCoche.idMarca = item.id;
    this.seleccionMarca = false;
    this.seleccionModelo = true;
  }

  setModelo() {
    this.datosCoche.modelo = this.nombreModelo;
    this.seleccionModelo = false;
    this.seleccionColor = true;
  }

  setColor(item) {
    this.datosCoche.colorAuto = item.color;
    this.seleccionColor = false;
    this.chapaAnho = true;
  }

  isDisabled(): boolean {
    if (this.anho != "" && this.numeroChapa != "") {
      return false;
    } else {
      return true;
    }
  }

  setChapa() {
    var chapa_mensaje = <any>document.getElementById("chapa_invalid");
    var data = {
      nroChapaAuto: this.numeroChapa
    };

    console.log(data);
    this.cocheService.checkChapa(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.datosCoche.nroChapaAuto = this.numeroChapa;
          this.datosCoche.anho = this.anho;
          this.chapaAnho = false;
          this.fotoCoche = true;
          console.log(response);
        } else {
          /*this.nuevoCoche = false;
          this.fotoCoche = false;*/
          this.mensajeChapa = response.mensaje;
          chapa_mensaje.style.display = "block";
        }
      }
    });
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    console.log(file);

    myReader.onloadend = e => {
      this.datosCoche.foto = myReader.result;
      var label = document.getElementById("label-foto-auto");
      label.innerHTML = file.name;
    };
    myReader.readAsDataURL(file);
  }

  registrarCoche() {
    //var chapa_mensaje = <any>document.getElementById("chapa_invalid");
    var data = this.datosCoche;
    console.log(data);
    this.ngxService.start();
    this.cocheService.registrarCoche(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.ngxService.stop();
          this.chapaAnho = false;
          this.confirmacionExito = true;
          this.fotoCoche = false;
          this.datosCoche = {};
          this.filtroMarca = "";
          this.nombreModelo = "";
          this.numeroChapa = "";
          this.anho = "";
          this.nombreColor = "";
          this.filteredItems = [];
          console.log(response);
        } else {
          this.ngxService.stop();
          this.nuevoCoche = false;
          this.fotoCoche = false;
          //chapa_mensaje.style.display = "block";
        }
      }
    });
  }

  validarChapa() {
    var chapa_mensaje = <any>document.getElementById("chapa_invalid");
    var data = {
      nroChapaAuto: this.numeroChapa
    };

    console.log(data);
    this.cocheService.checkChapa(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          /*this.chapaAnho = false;
          this.confirmacionExito = true;
          this.fotoCoche = false;*/
          console.log(response);
        } else {
          /*this.nuevoCoche = false;
          this.fotoCoche = false;*/
          this.mensajeChapa = response.mensaje;
          chapa_mensaje.style.display = "block";
        }
      }
    });
  }

  listarMarcas() {
    this.cocheService.listarMarcas().subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.marcas = response.data;
        } else {
          console.log(response);
        }
      }
    });
  }

  omitirFoto() {
    this.fotoCoche = false;
    this.registrarCoche();
  }

  aceptar() {
    this.confirmacionExito = false;
    this.nuevoCoche = false;
    this.getCoches();
  }

  desactivar(item) {
    this.cocheService.desactivarCoche(item).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.getCoches();
          console.log(response);
        } else {
          this.toastr.error("", response.mensaje);
        }
      }
    });
  }

  activar(item) {
    this.cocheService.activarCoche(item).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.getCoches();
          console.log(response);
        } else {
          this.toastr.error("", response.mensaje);
        }
      }
    });
  }

  subirFoto() {
    var data = this.uploadFoto;
    console.log(data);
    this.cocheService.subirFotoCoche(data).subscribe(response => {
      if (response) {
        this.ngxService.stop();
        if (response.estado == 0) {
          this.getCoches();
          console.log(response);
        } else {
          this.toastr.error("", response.mensaje);
          console.log(response);
        }
      }
    });
  }

  changeListenerPic($event): void {
    this.readThisPic($event.target);
  }

  readThisPic(inputValue: any): void {
    var file: File = inputValue.files[0];
    var ch = inputValue.id;
    var myReader: FileReader = new FileReader();

    myReader.onloadend = e => {
      this.uploadFoto.foto = myReader.result;
      this.uploadFoto.nroChapaAuto = ch;
      this.subirFoto();
    };
    myReader.readAsDataURL(file);
  }

  /* COCHE */

  ngOnInit() {
    if (this.router.url.includes("/coches")) {
      console.log("incluye /coches");
      var tabelement = document.getElementById("tab-coche");
      tabelement.classList.add("active");
      var divelement = document.getElementById("coche");
      divelement.classList.add("active", "show");
    }
    this.getCoches();
    this.assignCopy();
  }
}
