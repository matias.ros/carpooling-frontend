import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-info-personal",
  templateUrl: "./info-personal.component.html",
  styleUrls: ["./info-personal.component.scss"]
})
export class InfoPersonalComponent implements OnInit {
  user: any;
  constructor(private router: Router) {}

  ngOnInit() {
    if (
      this.router.url.includes("informacion-personal") ||
      this.router.url == "/perfil"
    ) {
      var tabelement = document.getElementById("tab-info-personal");
      tabelement.classList.add("active");
      var divelement = document.getElementById("info-personal");
      divelement.classList.add("active", "show");
    }
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
    console.log(this.user);
  }
}
