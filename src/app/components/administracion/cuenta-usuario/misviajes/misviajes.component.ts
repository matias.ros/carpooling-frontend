import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { BuscarViajesService } from 'src/app/services/buscar-viajes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-misviajes',
  templateUrl: './misviajes.component.html',
  styleUrls: ['./misviajes.component.scss']
})
export class MisviajesComponent implements OnInit {
  misviajes: any = [];
  constructor(private ngxService: NgxUiLoaderService,private toastr: ToastrService, private datePipe: DatePipe, private buscarviajesService: BuscarViajesService) { }

  ngOnInit() {
    this.getMisViajes();
  }
  getMisViajes() {
    this.buscarviajesService.listarTodosMisViajes().subscribe(response => {
      if (response.estado == 0) {
        this.misviajes = response.data;
      }
    });
  }
  getFechaFormat(fecha, hora) {
    var fechaSalidaAux = new Date(fecha.substr(6, 4), fecha.substr(3, 2) - 1, fecha.substr(0, 2), hora.substr(0, 2), hora.substr(3, 2));
    return this.datePipe.transform(fechaSalidaAux, "E, d MMMM, y, HH:mm");
  }
  cancelarViaje(id) {
    var data = {
      idViaje: id,
      motivoCancelacion: "cancelación"
    };
    this.ngxService.start();
    this.buscarviajesService.cancelarViaje(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.getMisViajes();
        } 
        this.ngxService.stop();
      }
    });

  }
}
