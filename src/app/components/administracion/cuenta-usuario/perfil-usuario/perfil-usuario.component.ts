import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-perfil-usuario",
  templateUrl: "./perfil-usuario.component.html",
  styleUrls: ["./perfil-usuario.component.scss"]
})
export class PerfilUsuarioComponent implements OnInit {
  user: any;
  aux = {};

  constructor(private router: Router) {}

  setRoute(path) {
    this.router.navigate(["/perfil/" + path]);
  }

  ngOnInit() {
    //this.router.navigate(["/perfil/informacion-personal"]);
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
    console.log(this.user);
  }
}
