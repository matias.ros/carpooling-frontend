import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ToastrService } from "ngx-toastr";
import { PreferenciasService } from "src/app/services/preferencias.service";

@Component({
  selector: "app-preferencias",
  templateUrl: "./preferencias.component.html",
  styleUrls: ["./preferencias.component.scss"]
})
export class PreferenciasComponent implements OnInit {
  preferencias: any = [];
  mascotas = 0;
  kids = 0;
  fumar = 0;
  showMensaje = false;

  constructor(
    private ngxService: NgxUiLoaderService,
    private router: Router,
    public preferenciaService: PreferenciasService,
    private toastr: ToastrService
  ) {}

  setFumar($event) {
    console.log("evento id", $event.target.id);
    this.fumar = $event.target.id.replace("fumar", "");
    var lista = document.getElementsByName("fumar");
    lista.forEach(obj => {
      if (obj.classList.contains("selected-pref")) {
        obj.classList.remove("selected-pref");
      }
    });
    $event.target.classList.add("selected-pref");
  }

  setKids($event) {
    console.log("evento id", $event.target.id);
    this.kids = $event.target.id.replace("kids", "");
    var lista = document.getElementsByName("kids");
    lista.forEach(obj => {
      if (obj.classList.contains("selected-pref")) {
        obj.classList.remove("selected-pref");
      }
    });
    $event.target.classList.add("selected-pref");
  }

  setMascotas($event) {
    console.log("evento id", $event.target.id);
    this.mascotas = $event.target.id.replace("mascotas", "");
    var lista = document.getElementsByName("pets");
    lista.forEach(obj => {
      if (obj.classList.contains("selected-pref")) {
        obj.classList.remove("selected-pref");
      }
    });
    $event.target.classList.add("selected-pref");
  }

  getPref() {
    this.ngxService.start();
    this.preferenciaService.listarPreferencias().subscribe(response => {
      if (response.estado == 0) {
        this.preferencias = response.data;
        this.setPreferenciasIniciales();
        this.ngxService.stop();
        console.log(response);
      } else {
        this.showMensaje = true;
        this.ngxService.stop();
      }
    });
  }

  setPreferenciasIniciales() {
    if (this.preferencias.preferenciaBebes == "NO") {
      this.kids = 3;
    }
    if (this.preferencias.preferenciaBebes == "PUEDE SER") {
      this.kids = 2;
    }
    if (this.preferencias.preferenciaBebes == "SI") {
      this.kids = 1;
    }
    if (this.preferencias.preferenciaMascota == "SI") {
      this.mascotas = 1;
    }
    if (this.preferencias.preferenciaMascota == "NO") {
      this.mascotas = 3;
    }
    if (this.preferencias.preferenciaMascota == "PUEDE SER") {
      this.mascotas = 2;
    }
    if (this.preferencias.preferenciaFumar == "PUEDE SER") {
      this.fumar = 2;
    }
    if (this.preferencias.preferenciaFumar == "NO") {
      this.fumar = 3;
    }
    if (this.preferencias.preferenciaFumar == "SI") {
      this.fumar = 1;
    }
  }

  validar() {
    if (this.mascotas == 0 || this.fumar == 0 || this.kids == 0) {
      this.toastr.error(
        "Necesitamos especificar todas tus preferencias",
        "Ups!"
      );
    } else {
      this.setPref();
    }
  }

  setPref() {
    var data = {
      idPreferenciaBebes: this.kids,
      idPreferenciaFumar: this.fumar,
      idPreferenciaMascotas: this.mascotas
    };
    console.log(data);
    this.ngxService.start();
    this.preferenciaService.setPreferencias(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.showMensaje = false;
          this.getPref();
          console.log(response);
          this.ngxService.stop();
        } else {
          this.toastr.error(response.mensaje);
          this.ngxService.stop();
        }
      }
    });
  }

  updatePref() {
    var data = {
      idPreferenciaBebes: this.kids,
      idPreferenciaFumar: this.fumar,
      idPreferenciaMascotas: this.mascotas
    };
    console.log(data);
    this.ngxService.start();
    this.preferenciaService.updatePreferencias(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.showMensaje = false;
          this.getPref();
          console.log(response);
          this.ngxService.stop();
        } else {
          console.log(response);
          this.ngxService.stop();
        }
      }
    });
  }

  ngOnInit() {
    this.showMensaje = false;
    if (this.router.url.includes("/preferencias")) {
      console.log("incluye /preferencias");
      var tabelement = document.getElementById("tab-preferencias");
      tabelement.classList.add("active");
      var divelement = document.getElementById("preferencias");
      divelement.classList.add("active", "show");
    }
    this.getPref();
  }
}
