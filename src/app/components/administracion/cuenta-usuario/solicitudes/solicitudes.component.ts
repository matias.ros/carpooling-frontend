import { Component, OnInit } from '@angular/core';
import { SolicitudesService } from 'src/app/services/solicitudes.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {
  solicitudes: any = [];
  cantSolicitudes: any = 0;
  obj_filtro: any = {};
  viajes: any = [];
  constructor(private ngxService: NgxUiLoaderService,private toastr: ToastrService,private datePipe: DatePipe, private solicitudService: SolicitudesService) { }

  ngOnInit() {
    this.getSolicitudes();
  }

  getFechaFormat(fecha, hora) {
    var fechaSalidaAux = new Date(fecha.substr(6, 4), fecha.substr(3, 2) - 1, fecha.substr(0, 2), hora.substr(0, 2), hora.substr(3, 2));
    return this.datePipe.transform(fechaSalidaAux, "E, d MMMM, y, HH:mm");
  }

  getSolicitudes() {
    this.ngxService.start();
    this.solicitudService.listarSolicitudes().subscribe(response => {
      this.ngxService.stop();
      if (response.estado == 0) {
        this.solicitudes = response.data;
        this.cantSolicitudes= 0;
        if(this.solicitudes){
          this.solicitudes.forEach(element => {
            this.cantSolicitudes = this.cantSolicitudes + element.solicitudes.length;
          });
          console.log("cant",this.cantSolicitudes)
        } 
      }
    });
  }

  getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  getFechaViaje(fechaCreacion) {
    if (fechaCreacion) {
      var fechaCreacion: any = fechaCreacion;
      var fechaSalidaAux = new Date(fechaCreacion.substr(6, 4), fechaCreacion.substr(3, 2) - 1, fechaCreacion.substr(0, 2));
      return this.datePipe.transform(fechaSalidaAux, " 'Usuario desde el ' d 'de' MMMM 'del' y");
    }
  }
  aprobarSolicitud(id) {
    var data = {
      "idSolicitud":id
    };
    this.solicitudService.aprobarSolicitud(data).subscribe(response => {
      if (response.estado == 0) {
        this.getSolicitudes();
        this.toastr.success("Solicitud aprobada con éxito");
      }else{
        this.toastr.error(response.mensaje);
      }
    });
  }
  
  rechazarSolicitud(id) {
    var data = {
      "idSolicitud":id
    };
    this.solicitudService.rechazarSolicitud(data).subscribe(response => {
      if (response.estado == 0) {
        this.getSolicitudes();
        this.toastr.info("Solicitud rechazada");
      }else{
        this.toastr.error(response.mensaje);
      }
    });
  }
}
