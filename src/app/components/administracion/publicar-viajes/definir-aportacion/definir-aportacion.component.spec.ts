import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinirAportacionComponent } from './definir-aportacion.component';

describe('DefinirAportacionComponent', () => {
  let component: DefinirAportacionComponent;
  let fixture: ComponentFixture<DefinirAportacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinirAportacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinirAportacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
