import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { PRECIO_NAFTA, CONSUMO_PROM, EMISION_ESTANDAR_CO2, CANTIDAD_PASAJEROS_PROM } from "../../../../config/constants";

@Component({
  selector: 'app-definir-aportacion',
  templateUrl: './definir-aportacion.component.html',
  styleUrls: ['./definir-aportacion.component.scss']
})
 
export class DefinirAportacionComponent implements OnInit {
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;
  map: google.maps.Map;
  lat = -25.311951;
  lng = -57.587114;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  emisionCo2: any;
  costoIrSolo: any;
  costoUnitarioPasajero: any;
  topePrecio: any;
  plazas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  lugares: any = [];
  lugarOrigen: string;
  lugarDestino: string;
  origenes: any = [];
  destinos: any = [];
  mapOptions: google.maps.MapOptions = {
    disableDefaultUI: false,
    mapTypeControl: false,
    streetViewControl: false,
    scaleControl: true,
    zoomControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: this.coordinates,
    zoom: 13,
    styles: [
      {
        featureType: "landscape",
        stylers: [
          {
            hue: "#FFBB00"
          },
          {
            saturation: 43.400000000000006
          },
          {
            lightness: 37.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.highway",
        stylers: [
          {
            hue: "#FFC200"
          },
          {
            saturation: -61.8
          },
          {
            lightness: 45.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.arterial",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 51.19999999999999
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.local",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 52
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "water",
        stylers: [
          {
            hue: "#0078FF"
          },
          {
            saturation: -13.200000000000003
          },
          {
            lightness: 2.4000000000000057
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "poi",
        stylers: [
          {
            hue: "#00FF6A"
          },
          {
            saturation: -1.0989010989011234
          },
          {
            lightness: 11.200000000000017
          },
          {
            gamma: 1
          }
        ]
      }
    ]
  };

  @Input() public viaje_obj: any=[];
  @Input() public ciudadesDePaso: any=[];
  @Output() public routeOutput = new EventEmitter<boolean>();

  redirectForm(){
    var trayecto_section = document.getElementById("id_trayecto_section").style.display="block";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display="none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display="none";
    window.scroll(0,0);
  }
  
  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.setAllMarkers();
  }

  constructor() {
    if (false) {
      
      this.viaje_obj = JSON.parse(sessionStorage.getItem("obj_viaje"))
      var distanciasDividas = this.viaje_obj.distanciasDivididas

      this.calcularCo2Ahorrado();
      this.calcularCostoViaje();

      if (this.viaje_obj.ciudadesPaso.length > 0) {
        this.lugares = [...this.viaje_obj.ciudadesPaso];
        var origen = this.viaje_obj.lugarOrigen;
        var destino = ""
        var index = 0
        var distancia: any
        this.lugares.forEach(element => {
          distancia = 0
          if (distanciasDividas[index]) {
            distancia = distanciasDividas[index] / 1000
            distancia = parseInt(distancia)
          }
          destino = element.lugar;
          this.ciudadesDePaso.push({ "origen": origen, "destino": destino, "distancia": distancia })
          origen = element.lugar;
          index += 1
        });
        console.log(index)
        if (distanciasDividas[index]) {
          distancia = distanciasDividas[index] / 1000
          distancia = parseInt(distancia)
        }
        destino = this.viaje_obj.lugarDestino;
        this.ciudadesDePaso.push({ "origen": origen, "destino": destino, "distancia": distancia })
      }
    }

    console.log(this.ciudadesDePaso)

  }



  setAllMarkers() {

    var latlngbounds = new google.maps.LatLngBounds();
    var markerInicio = new google.maps.Marker({
      position: { lat: this.viaje_obj.latitudOrigen, lng: this.viaje_obj.longitudOrigen },
      map: this.map
    });
    latlngbounds.extend(new google.maps.LatLng(markerInicio.getPosition().lat(), markerInicio.getPosition().lng()));

    var markerFin = new google.maps.Marker({
      position: { lat: this.viaje_obj.latitudDestino, lng: this.viaje_obj.longitudDestino },
      map: this.map
    });
    latlngbounds.extend(new google.maps.LatLng(markerFin.getPosition().lat(), markerFin.getPosition().lng()));

    markerInicio.setMap(this.map);
    markerFin.setMap(this.map);

    if (this.viaje_obj.ciudadesPaso.length > 0) {
      this.viaje_obj.ciudadesPaso.forEach(element => {
        var markerDePaso = new google.maps.Marker({
          position: { lat: element.latitud, lng: element.longitud },
          map: this.map
        });
        latlngbounds.extend(new google.maps.LatLng(markerDePaso.getPosition().lat(), markerDePaso.getPosition().lng()));
        markerDePaso.setMap(this.map)
      });
    }
    this.map.fitBounds(latlngbounds);
  }

  calcularCo2Ahorrado() {
    var distancia = this.viaje_obj.kilmetrosViaje.value
    this.emisionCo2 = distancia * CONSUMO_PROM * EMISION_ESTANDAR_CO2 / 100
    this.emisionCo2 = Math.round(this.emisionCo2) + " Kg"
  }

  changePrice($event) {
    var source = $event.target
    var id_element = $event.target.id.substr(0, 1)
    var distanciasDividas = this.viaje_obj.distanciasDivididas
    var topeMaximo = this.calcularTopeMax(distanciasDividas[id_element] / 1000)
    console.log(topeMaximo)
    var price_invalid = (<any>document.getElementById("price_invalid"))
    if (source.value >= topeMaximo) {
      $event.target.classList.remove("price-valid")
      $event.target.classList.add("price-invalid")
      price_invalid.style.display = "block"
    } else {
      $event.target.classList.add("price-valid")
      $event.target.classList.remove("price-invalid")
      price_invalid.style.display = "none"
    }
  }

  calcularTopeMax(distancia) {
    var costoIrSolo = (PRECIO_NAFTA * CONSUMO_PROM * distancia) / 100
    var costoUnitarioPasajero: any = costoIrSolo / CANTIDAD_PASAJEROS_PROM
    costoUnitarioPasajero = parseInt(costoUnitarioPasajero)
    var topePrecio: any = costoUnitarioPasajero * 1.5
    topePrecio = parseInt(topePrecio)
    return topePrecio;
  }

  calcularCostoViajePorDistancia(distancia) {
    var costoIrSolo = (PRECIO_NAFTA * CONSUMO_PROM * distancia) / 100
    var costoUnitario: any = costoIrSolo / CANTIDAD_PASAJEROS_PROM
    costoUnitario = parseInt(costoUnitario)
    return costoUnitario
  }

  calcularCostoViaje() {
    this.costoIrSolo = (PRECIO_NAFTA * CONSUMO_PROM * this.viaje_obj.kilmetrosViaje) / 100
    this.costoUnitarioPasajero = this.costoIrSolo / CANTIDAD_PASAJEROS_PROM
    this.costoUnitarioPasajero = parseInt(this.costoUnitarioPasajero)
    this.topePrecio = this.costoUnitarioPasajero * 1.5
    this.topePrecio = parseInt(this.topePrecio)
  }

  selectSize(size) {
    var sizes = (<any>document.getElementsByClassName("sizes"))
    if (size == 1) {
      if (!sizes[0].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
      } 
    } else if (size == 2) {
      if (!sizes[1].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
      } else if (!sizes[2].classList.contains("selected-size")) {
        sizes[1].classList.remove("selected-size");
      }
    } else if (size == 3) {
      if (!sizes[2].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
        sizes[2].classList.add("selected-size");
      } else {
        sizes[2].classList.remove("selected-size");
      }
    }
  }

  getAllData(){
    var costo_unitario = (<any>document.getElementById("0_price")).value;
    var plazas_cant = (<any>document.getElementById("id_plaza_cant")).value;
    var equipajeSize = (<any>document.getElementsByClassName("selected-size")).length;
    var maxDosAtras = (<any>document.getElementById("check-3")).checked;
    var solicitudTrayectoParcial = (<any>document.getElementById("check-2")).checked;
    var desvio = (<any>document.getElementById("id_desvio")).value;
    var datosAdicionales = (<any>document.getElementById("id_datosAdicionales")).value;
    this.viaje_obj["costo_unitario"]=costo_unitario
    this.viaje_obj["plazas_cant"]=plazas_cant
    this.viaje_obj["equipajeSize"]=equipajeSize
    this.viaje_obj["maxDosAtras"]=maxDosAtras
    this.viaje_obj["solicitudTrayectoParcial"]=solicitudTrayectoParcial
    this.viaje_obj["desvio"]=desvio
    this.viaje_obj["datosAdicionales"]=datosAdicionales
    var trayecto_section = document.getElementById("id_trayecto_section").style.display="none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display="none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display="block";
    window.scroll(0,0);
  }

  ngOnInit() {
  }
  // ngAfterViewInit() {
  //   var viaje_obj = sessionStorage.getItem("obj_viaje")
  //   if(viaje_obj){
  //     console.log("Existen datos de un viaje existente")
  //   }
  //   this.mapInitializer();

  // }

}
