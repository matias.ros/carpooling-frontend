import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinirTrayectoComponent } from './definir-trayecto.component';

describe('DefinirTrayectoComponent', () => {
  let component: DefinirTrayectoComponent;
  let fixture: ComponentFixture<DefinirTrayectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefinirTrayectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinirTrayectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
