import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, Input, EventEmitter, Output, DoCheck, IterableDiffers } from '@angular/core';
import { Router } from '@angular/router';
import { PRECIO_NAFTA, CONSUMO_PROM, CANTIDAD_PASAJEROS_PROM, EMISION_ESTANDAR_CO2 } from 'src/app/config/constants';
import { DefinirAportacionComponent } from '../definir-aportacion/definir-aportacion.component';
import { RegistroService } from 'src/app/services/registro.service';
import { CocheService } from 'src/app/services/coche.service';
import { debug } from 'util';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-definir-trayecto',
  templateUrl: './definir-trayecto.component.html',
  styleUrls: ['./definir-trayecto.component.scss']
})

export class DefinirTrayectoComponent implements OnInit, AfterViewInit {
  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;
  @ViewChild(DefinirAportacionComponent, { static: false }) aportacionReference;
  @Input() name: string = '';
  @Input() model: any;
  @Input() placeholder: string = 'DD/MM/YYYY';
  @Input() placeholderHora: string = 'HH:MM';
  @Input() type: string = 'calendar';
  @Input() type2: string = 'timer';
  @Output() dateChange = new EventEmitter();
  valorPrueba: string = "Antes de tener valor "
  map: google.maps.Map;
  show_trayecto: boolean = true;
  lat = -25.311951;
  lng = -57.587114;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  autocomplete;
  autocomplete1;
  autocompleteDePaso: any = []
  coordenadas: any = [];
  coordenadasDePaso: any = [];
  markers: any = [];
  markersDePaso: any = [];
  inicio_isvalid: boolean = false;
  fin_isvalid: boolean = false;
  lugares_isvalid: boolean = false;
  horario_isvalid: boolean = false;
  obj_viaje: any = {}
  ciudadesDePaso: any = [];
  viaje_obj: any = {};
  plazas = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  emisionCo2: any;
  onlyMujeres:any = false;
  public direcciones: any[] = [{
    ciudadDePaso: ''
  }];
  mapOptions: google.maps.MapOptions = {
    disableDefaultUI: false,
    mapTypeControl: false,
    streetViewControl: false,
    scaleControl: true,
    zoomControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: this.coordinates,
    zoom: 13,
    styles: [
      {
        featureType: "landscape",
        stylers: [
          {
            hue: "#FFBB00"
          },
          {
            saturation: 23.400000000000006
          },
          {
            lightness: 17.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.highway",
        stylers: [
          {
            hue: "#FFC200"
          },
          {
            saturation: -61.8
          },
          {
            lightness: 45.599999999999994
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.arterial",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 51.19999999999999
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "road.local",
        stylers: [
          {
            hue: "#FF0300"
          },
          {
            saturation: -100
          },
          {
            lightness: 52
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "water",
        stylers: [
          {
            hue: "#0078FF"
          },
          {
            saturation: -13.200000000000003
          },
          {
            lightness: 2.4000000000000057
          },
          {
            gamma: 1
          }
        ]
      },
      {
        featureType: "poi",
        stylers: [
          {
            hue: "#009b72"
          },
          // {
          //   saturation: -1.0989010989011234
          // },
          // {
          //   lightness: 11.200000000000017
          // },
          // {
          //   gamma: 1
          // }
        ]
      }
    ]
  };

  icon = {
    url: "assets/img/icons/theme/map/marker-3.png", // url
    scaledSize: new google.maps.Size(50, 50), // scaled size
  };

  marker = new google.maps.Marker({
    position: this.coordinates,
    map: this.map,
    icon: this.icon
  });

  exampleMethodParent($event) {
    this.show_trayecto = $event
  }
  salirMapa(){
    
  }
  eliminaMakerDePaso(e) {
    var element = []
    element.push(e.target)
    var index = (<any>element)[0].id.substr(8, 1)
    if (this.markersDePaso[index]) {
      this.markersDePaso[index].setMap(null)
      document.getElementById("section_" + index).remove()
    }

  }
  mapInitializer() {

    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    var inputSalida = document.getElementById('pac-input');
    var inputLlegada = document.getElementById('pac-input1');
    var inputdePaso = document.getElementById('id_address_0');

    //Init autocomplete
    this.autocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>inputSalida);
    this.autocomplete1 = new google.maps.places.Autocomplete(<HTMLInputElement>inputLlegada);
    this.autocompleteDePaso.push(new google.maps.places.Autocomplete(<HTMLInputElement>inputdePaso));

    google.maps.event.addListener(this.autocomplete, 'place_changed', (function (map, coordenadas, markers, inicio_isvalid) {
      return function () {
        var place = this.getPlace();
        inicio_isvalid = false
        if (place.geometry) {
          var marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map
          });
          marker.setMap(map);
          markers[0] = marker
          coordenadas[0] = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng())
          var latlngbounds = new google.maps.LatLngBounds();
          if (markers[0] != "" && markers[1] != "") {
            coordenadas.forEach(element => {
              latlngbounds.extend(element);
            });

            map.fitBounds(latlngbounds);
          } else {
            map.setCenter({ lat: marker.getPosition().lat(), lng: marker.getPosition().lng() });
          }
          var fecha_ida = (<any>document.getElementById("ida_invalid"))

          if ((place.types.indexOf("point_of_interest") > -1) || (place.types.indexOf("premise") > -1) || (place.types.indexOf("subpremise") > -1) || (place.types.indexOf("intersection") > -1)) {
            fecha_ida.style.display = "none"
          } else {
            fecha_ida.style.display = "block"
          }

        }
      };
    })(this.map, this.coordenadas, this.markers, this.inicio_isvalid));

    google.maps.event.addListener(this.autocomplete1, 'place_changed', (function (map, coordenadas, markers, fin_isvalid) {
      return function () {
        fin_isvalid = false
        var place = this.getPlace();
        if (place.geometry) {

          var marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map
          });

          marker.setMap(map);
          markers[1] = marker
          coordenadas[1] = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng())
          var latlngbounds = new google.maps.LatLngBounds();
          if (markers[0] != "" && markers[1] != "") {
            coordenadas.forEach(element => {
              latlngbounds.extend(element);
            });

            map.fitBounds(latlngbounds);
          } else {
            map.setCenter({ lat: marker.getPosition().lat(), lng: marker.getPosition().lng() });
          }

          var fecha_vuelta = (<any>document.getElementById("vuelta_invalid"))
          if ((place.types.indexOf("point_of_interest") > -1) || (place.types.indexOf("premise") > -1) || (place.types.indexOf("subpremise") > -1) || (place.types.indexOf("intersection") > -1)) {
            fecha_vuelta.style.display = "none"
          } else {
            fecha_vuelta.style.display = "block"
          }
        }
      };
    })(this.map, this.coordenadas, this.markers, this.fin_isvalid));

    google.maps.event.addListener(this.autocompleteDePaso[0], 'place_changed', (function (map, coordenadasDePaso, markersDePaso, markers, coordenadas, direcciones, autocompleteDePaso) {
      return function () {
        var place = this.getPlace();
        if (place.geometry) {

          var marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map
          });

          marker.setMap(map);
          if (markersDePaso[0] && markersDePaso[0] != "") {
            markersDePaso[0] = marker;
          } else {
            markersDePaso.push(marker);
          }
          console.log(markersDePaso)
          coordenadasDePaso.push(new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng()))

          var latlngbounds = new google.maps.LatLngBounds();
          if (markers[0] != "" && markers[1] != "") {
            coordenadasDePaso.forEach(element => {
              latlngbounds.extend(element);
            });
            coordenadas.forEach(element => {
              latlngbounds.extend(element);
            });

            map.fitBounds(latlngbounds);
          }
          var lugar_paso = (<any>document.getElementById("lugar_invalid_0"))
          var addInput = true
          var input_paso = (<any>document.getElementsByClassName("lugar-paso"))

          input_paso.forEach(element => {
            if (element.value == "") {
              addInput = false
            }
          });

          if ((place.types.indexOf("point_of_interest") > -1) || (place.types.indexOf("premise") > -1) || (place.types.indexOf("subpremise") > -1) || (place.types.indexOf("intersection") > -1)) {
            if (lugar_paso) {
              lugar_paso.style.display = "none"
            }

            var section = document.createElement("section");
            section.setAttribute("style", "padding:0px !important");
            section.setAttribute("class", "ciudad-paso")
            var id = autocompleteDePaso.length
            section.setAttribute("id", "section_" + id)
            var input = document.createElement("input");
            var p = document.createElement("p");
            p.setAttribute("id", "x-times-" + id)
            p.setAttribute("class", "icon-times")
            var times = document.createTextNode("x");
            p.appendChild(times);
            input.setAttribute("id", "id_address_" + id)
            input.setAttribute("class", "form-control lugar-paso")
            input.setAttribute("placeholder", "Ejemplo Terminal de Omnibus Ciudad del Este, Ciudad del Este, Paraguay")
            input.setAttribute("name", id + "_address")

            var p_mensaje = document.createElement("p");
            p_mensaje.setAttribute("class", "mensaje_rutaInvalid")
            p_mensaje.setAttribute("id", "lugar_invalid_" + id)
            p_mensaje.setAttribute("style", "display:none;")
            var mensaje = document.createTextNode("¡Escribe un lugar concreto para que podamos recomendar tu viaje a usuarios de la zona!");
            p_mensaje.appendChild(mensaje);

            p.addEventListener('click', function (e) {
              var element = []
              element.push(e.target)
              var index = (<any>element)[0].id.substr(8, 1)
              if (markersDePaso[index] && markersDePaso[index] != "") {
                markersDePaso[index].setMap(null)
                markersDePaso[index] = "";
                document.getElementById("section_" + index).remove()
              }
            })

            input.addEventListener('keyup', function (e) {
              var elementId = (<any>e.srcElement).id.substr(11, 1);
              console.log("elementId", elementId)
              if (markersDePaso[elementId] != "" && markersDePaso[elementId]) {
                markersDePaso[elementId].setMap(null)
                markersDePaso[elementId] = ""
              }
              console.log(markersDePaso)
            })

            section.appendChild(input)
            section.appendChild(p)
            section.appendChild(p_mensaje)
            var form = document.getElementById("container-form")
            if (addInput) {
              form.appendChild(section);
              var inputdePaso = document.getElementById("id_address_" + id)
              var autocomplete_object = new google.maps.places.Autocomplete(<HTMLInputElement>inputdePaso)
              autocomplete_object["id_autocomplete"] = id;
              autocompleteDePaso.push(autocomplete_object);
              autocomplete_object.addListener('place_changed', changeAutocomplete)
            }



            function changeAutocomplete() {

              var place = this.getPlace();

              if (place.geometry) {

                var marker = new google.maps.Marker({
                  position: place.geometry.location,
                  map: map
                });

                marker.setMap(map);
                if (markersDePaso[this.id_autocomplete] == "") {
                  markersDePaso[this.id_autocomplete] = marker;
                } else {
                  markersDePaso.push(marker);
                }
                coordenadasDePaso.push(new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng()))
                var latlngbounds = new google.maps.LatLngBounds();
                if (markers[0] != "" && markers[1] != "") {
                  coordenadasDePaso.forEach(element => {
                    latlngbounds.extend(element);
                  });
                  coordenadas.forEach(element => {
                    latlngbounds.extend(element);
                  });
                  map.fitBounds(latlngbounds);
                }

                var index_autocomplete = autocompleteDePaso.length - 1
                var lugar_paso = (<any>document.getElementById("lugar_invalid_" + index_autocomplete))
                var addInput = true;
                var input_paso = (<any>document.getElementsByClassName("lugar-paso"))

                input_paso.forEach(element => {
                  if (element.value == "") {
                    addInput = false;
                  }
                })

                if ((place.types.indexOf("point_of_interest") > -1) || (place.types.indexOf("premise") > -1) || (place.types.indexOf("subpremise") > -1) || (place.types.indexOf("intersection") > -1)) {

                  if (lugar_paso) {
                    lugar_paso.style.display = "none"
                  }

                  var section = document.createElement("section");
                  section.setAttribute("style", "padding:0px !important");
                  section.setAttribute("class", "ciudad-paso")
                  var id = autocompleteDePaso.length
                  section.setAttribute("id", "section_" + id)
                  var input = document.createElement("input");
                  var p = document.createElement("p");
                  p.setAttribute("id", "x-times-" + id)
                  p.setAttribute("class", "icon-times")
                  var times = document.createTextNode("x");
                  p.appendChild(times);
                  input.setAttribute("id", "id_address_" + id)
                  input.setAttribute("class", "form-control lugar-paso")
                  input.setAttribute("placeholder", "Ejemplo Terminal de Omnibus Ciudad del Este, Ciudad del Este, Paraguay")
                  input.setAttribute("name", id + "_address")

                  var p_mensaje = document.createElement("p");
                  p_mensaje.setAttribute("class", "mensaje_rutaInvalid")
                  p_mensaje.setAttribute("id", "lugar_invalid_" + id)
                  p_mensaje.setAttribute("style", "display:none;")
                  var mensaje = document.createTextNode("¡Escribe un lugar concreto para que podamos recomendar tu viaje a usuarios de la zona!");
                  p_mensaje.appendChild(mensaje);

                  p.addEventListener('click', function (e) {
                    var element = []
                    element.push(e.target)
                    var index = (<any>element)[0].id.substr(8, 1)
                    if (markersDePaso[index] && markersDePaso[index] != "") {
                      markersDePaso[index].setMap(null)
                      markersDePaso[index] = "";
                      document.getElementById("section_" + index).remove()
                    }
                  })

                  input.addEventListener('keyup', function (e) {
                    var elementId = (<any>e.srcElement).id.substr(11, 1);
                    console.log("elementId", elementId)
                    if (markersDePaso[elementId] != "" && markersDePaso[elementId]) {
                      markersDePaso[elementId].setMap(null)
                      markersDePaso[elementId] = ""
                    }
                    console.log(markersDePaso)
                  })

                  section.appendChild(input)
                  section.appendChild(p)
                  section.appendChild(p_mensaje)

                  var form = document.getElementById("container-form")
                  if (addInput) {
                    form.appendChild(section);
                    var inputdePaso = document.getElementById("id_address_" + id)
                    var autocomplete_object = new google.maps.places.Autocomplete(<HTMLInputElement>inputdePaso)
                    autocompleteDePaso["id_autocomplete"] = id
                    autocompleteDePaso.push(autocomplete_object);
                    autocomplete_object.addListener('place_changed', changeAutocomplete)
                  }

                } else {
                  lugar_paso.style.display = "block"
                }

              }
            }
          } else {
            lugar_paso.style.display = "block"
          }

        }
      };
    })(this.map, this.coordenadasDePaso, this.markersDePaso, this.markers, this.coordenadas, this.direcciones, this.autocompleteDePaso));
    //---- termina autcomplete ----//
  }

  changeMarkerInicio($event) {

    if (this.markers[0] != "") {
      this.markers[0].setMap(null)
      this.markers[0] = ""
    }
    if (this.markers[1] != "") {
      this.map.setCenter({ lat: this.markers[1].getPosition().lat(), lng: this.markers[1].getPosition().lng() });
      this.map.setZoom(10);
    }

    if (this.markers[0] == "" && this.markers[1] == "") {
      this.map.setCenter({ lat: this.lat, lng: this.lng });
      this.map.setZoom(10);
    }
  }

  changeMarkerFin($event) {
    if (this.markers[1] != "") {
      this.markers[1].setMap(null)
      this.markers[1] = ""
    }
    if (this.markers[0] != "") {
      this.map.setCenter({ lat: this.markers[0].getPosition().lat(), lng: this.markers[0].getPosition().lng() });
      this.map.setZoom(10);
    }


    if (this.markers[0] == "" && this.markers[1] == "") {
      this.map.setCenter({ lat: this.lat, lng: this.lng });
      this.map.setZoom(10);
    }
  }

  changeMarkerDePaso($event) {
    var index = 0
    if (this.markersDePaso[index] != "" && this.markersDePaso[index]) {
      this.markersDePaso[index].setMap(null)
    }
  }

  getAllDataPrueba() {
    this.obj_viaje = { "idaYvuelta": false, "fechaViajeIda": "31/01/2020 21:41:00", "fechaIda": "31/01/2020", "horaIda": "21:41", "fechaViajeVuelta": "", "fechaVuelta": "", "horaVuelta": "", "lugarOrigenFormateado": "Asunción", "lugarOrigen": "Terminal de Ómnibus de Asunción, Asunción, Paraguay", "latitudOrigen": -25.3259833, "lugarFinFormateado": "Concepción", "longitudOrigen": -57.59609340000001, "lugarDestino": "Terminal De Omnibus, Concepción, Paraguay", "latitudDestino": -23.3985478, "longitudDestino": -57.44451919999999, "ciudadesPaso": [{ "lugar": "TERMINAL CIUDAD DEL ESTE, Terminal de Omnibus, Ciudad del Este, Paraguay", "latitud": -25.527788, "longitud": -54.61438399999999 }, { "lugar": "Universidad Nacional de Asunción, Campus Universitario, San Lorenzo, Paraguay", "latitud": -25.3357603, "longitud": -57.51973979999999 }, { "lugar": "TERMINAL DE OMNIBUS, San Joaquín, Caaguazú, Paraguay", "latitud": -25.0309377, "longitud": -56.0514729 }], "duracionViaje": 1171, "duracionViajeFormateado": "19 h 31 min", "duracionViajeConTrafico": 1153, "kilmetrosViaje": 1178, "costoTotalViaje": 236778, "distanciasDivididas": [326539, 317408, 202900, 330753, 1177600] }
    this.ciudadesDePaso = [
      { origen: "Terminal de Omnibus - Asuncion, Avenue Fernando de la Mora, Asunción, Paraguay", destino: "Shopping del Sol, Asunción, Paraguay", distancia: 6 }
      , { origen: "Shopping del Sol, Asunción, Paraguay", destino: "Terminal De Omnibus Mendoza, Mendoza, Argentina", distancia: 1799 }
      , { origen: "Terminal De Omnibus Mendoza, Mendoza, Argentina", destino: "Estación Terminal de Ómnibus de Formosa, Formosa, Paraguay", distancia: 1616 }
      , { origen: "Estación Terminal de Ómnibus de Formosa, Formosa, Paraguay", destino: "Terminal De Omnibus, Concepción, Paraguay", distancia: 539 }]
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "block";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "none";
    window.scroll(0, 0);
  }

  getAllData() {
    this.valorPrueba = "Antes de tener valor"
    this.obj_viaje["costoTotalViaje"] = 0
    this.ciudadesDePaso = []
    this.show_trayecto = true;
    var t0 = performance.now();
    //Inio y fin
    var inicio_formateado = ""
    if (this.markers[0] != "" && this.markers[1] != "") {
      if (this.autocomplete) {
        inicio_formateado = this.autocomplete.getPlace().vicinity
      }
      var inicio_viaje = {
        "lugar": (<any>document.getElementById("pac-input")).value,
        "latitud": this.markers[0].getPosition().lat(),
        "longitud": this.markers[0].getPosition().lng()
      }
      this.obj_viaje["lugarOrigen"] = inicio_viaje.lugar;
      this.obj_viaje["latitudOrigen"] = inicio_viaje.latitud;
      this.obj_viaje["longitudOrigen"] = inicio_viaje.longitud
      var fin_formateado = ""
      if (this.autocomplete1) {
        fin_formateado = this.autocomplete1.getPlace().vicinity
      }
      var fin_viaje = {
        "lugar": (<any>document.getElementById("pac-input1")).value,
        "latitud": this.markers[1].getPosition().lat(),
        "longitud": this.markers[1].getPosition().lng()
      }

      this.obj_viaje["lugarDestino"] = fin_viaje.lugar;
      this.obj_viaje["latitudDestino"] = fin_viaje.latitud;
      this.obj_viaje["longitudDestino"] = fin_viaje.longitud;
      this.obj_viaje["lugarOrigenFormateado"] = inicio_formateado;
      this.obj_viaje["lugarFinFormateado"] = fin_formateado;
      this.obj_viaje["idaYvuelta"] = (<any>document.getElementById("check-1")).checked
      //Se obtienen fechas y horas del viaje
      var fecha_ida = (<any>document.getElementById("id_fechaIda")).value
      var fecha_vuelta = (<any>document.getElementById("id_fechaVuelta")).value
      var hora_ida = (<any>document.getElementById("id_horaIda")).value
      var hora_vuelta = (<any>document.getElementById("id_horaVuelta")).value
      if (fecha_ida != "" && fecha_vuelta != "" && hora_ida != "" && hora_vuelta != "") {
        var date_ida = new Date(fecha_ida.substr(6, 4), fecha_ida.substr(3, 2), fecha_ida.substr(0, 2))
        var date_vuelta = new Date(fecha_vuelta.substr(6, 4), fecha_vuelta.substr(3, 2), fecha_vuelta.substr(0, 2))
        if (date_ida <= date_vuelta) {
          this.horario_isvalid = true
        } else {
          this.horario_isvalid = false
        }
      } else if (fecha_ida != "" && hora_ida != "") {
        this.horario_isvalid = true
      } else if (fecha_ida == "" && fecha_vuelta == "" && hora_ida == "" && hora_vuelta == "") {
        this.horario_isvalid = false
      }


      //Validaciones de lugares ingresados para asegurarnos que sean validos
      if (this.autocomplete && this.autocomplete1) {
        var lugar_salida = this.autocomplete.getPlace()
        var lugar_llegada = this.autocomplete1.getPlace()

        if ((lugar_salida.types.indexOf("point_of_interest") > -1) || (lugar_salida.types.indexOf("premise") > -1) || (lugar_salida.types.indexOf("subpremise") > -1) || (lugar_salida.types.indexOf("intersection") > -1)) {
          this.inicio_isvalid = true
        } else {
          this.inicio_isvalid = false
        }

        if ((lugar_llegada.types.indexOf("point_of_interest") > -1) || (lugar_llegada.types.indexOf("premise") > -1) || (lugar_llegada.types.indexOf("subpremise") > -1) || (lugar_llegada.types.indexOf("intersection") > -1)) {
          this.fin_isvalid = true
        } else {
          this.fin_isvalid = false
        }

        var lugares_dePaso = []
        var origenes = []
        var destinos = []
        var index;
        var lugares_paso = (<any>document.getElementsByClassName("lugar-paso"));
        if (this.markers[0] != "") {
          origenes.push(new google.maps.LatLng(inicio_viaje.latitud, inicio_viaje.longitud))
        }

        if (lugares_paso) {

          lugares_paso.forEach(element => {

            index = element.id.substr(11, 1)
            var autocomplete_object = this.autocompleteDePaso[index]
            if (autocomplete_object) {
              var place = autocomplete_object.getPlace()
              if (place) {
                if (place.types) {
                  if ((place.types.indexOf("point_of_interest") > -1) || (place.types.indexOf("premise") > -1) || (place.types.indexOf("subpremise") > -1) || (place.types.indexOf("intersection") > -1)) {
                    this.lugares_isvalid = true
                  } else {
                    this.lugares_isvalid = false
                  }
                }
              } else {
                this.lugares_isvalid = true
              }

            }
            //Se crea objetos de lugares de paso
            if (element.value != "") {
              lugares_dePaso.push({
                "lugar": element.value,
                "latitud": this.markersDePaso[index].getPosition().lat(),
                "longitud": this.markersDePaso[index].getPosition().lng()
              })
              origenes.push(new google.maps.LatLng(this.markersDePaso[index].getPosition().lat(), this.markersDePaso[index].getPosition().lng()))
              destinos.push(new google.maps.LatLng(this.markersDePaso[index].getPosition().lat(), this.markersDePaso[index].getPosition().lng()))
            }
          });
        }
        if (lugares_dePaso.length > 0) {
          var input_aportacion = document.getElementById("0_price").setAttribute("disabled", "disabled");
        }
        this.obj_viaje["ciudadesPaso"] = lugares_dePaso;
        if (this.markers[1] != "") {
          destinos.push(new google.maps.LatLng(fin_viaje.latitud, fin_viaje.longitud))
        }
      }
    }

    //Obtener duracion total del viaje y kilometros
    //Cada trayecto del viaje una cantidad de km diferente
    var distanciasDividas = []
    var duracion_viaje: any = 0
    var duracion_viaje_trafico: any = 0
    var kilometros_viaje: any = 0
    var formatDate = ""

    if (fecha_ida) {
      formatDate = fecha_ida.substr(6, 4) + "-" + fecha_ida.substr(3, 2) + "-" + fecha_ida.substr(0, 2) + "T" + hora_ida + ":00"
    }

    this.ngxService.start();
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: origenes,
        destinations: destinos,
        drivingOptions: {
          departureTime: new Date(formatDate),  // for the time N milliseconds from now.
        },
        travelMode: google.maps.TravelMode.DRIVING,
        avoidHighways: false,
        avoidTolls: false,
      },
      (response, status) => {
        this.valorPrueba = "Ya tengo valor antes de cargar ciudades"
        this.ngxService.stop();
        // return data back to component
        console.log(response, status, this.markersDePaso)
        if (response) {
          if (response.rows) {
            for (let index = 0; index < response.rows.length; index++) {

              if (response.rows[index]) {
                if (response.rows[index].elements) {
                  var array = response.rows[index].elements
                  if (array[index]) {
                    duracion_viaje += array[index].duration.value
                    if (array[index].duration_in_traffic) {
                      duracion_viaje_trafico += array[index].duration_in_traffic.value
                    }
                    kilometros_viaje += array[index].distance.value
                    var aux: any = array[index].distance.value
                    aux = Math.ceil(aux)
                    distanciasDividas.push(aux)
                  }
                }
              }
            }

            distanciasDividas.push(kilometros_viaje)
            if (kilometros_viaje != 0) {
              kilometros_viaje = kilometros_viaje / 1000
              kilometros_viaje = Math.ceil(kilometros_viaje)
            }
            this.obj_viaje["kilmetrosViaje"] = kilometros_viaje;
            this.obj_viaje["distanciasDivididas"] = distanciasDividas;
            this.obj_viaje["co2Ahorrado"] = this.calcularCo2Ahorrado();
            if (duracion_viaje_trafico != 0) {
              duracion_viaje_trafico = duracion_viaje_trafico / 60
              duracion_viaje_trafico = parseInt(duracion_viaje_trafico)
            }
            if (duracion_viaje != 0) {
              duracion_viaje = duracion_viaje / 60
              duracion_viaje = parseInt(duracion_viaje)
            }
          }
        }

        var timeFormat = this.timeConvert(duracion_viaje)
        //Validacion del formulario
        if (this.inicio_isvalid && this.fin_isvalid && this.horario_isvalid && this.lugares_isvalid) {

          this.show_trayecto = false;
          var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
          var trayecto_section = document.getElementById("id_aportacion_section").style.display = "block";
          var trayecto_section = document.getElementById("id_reserva_section").style.display = "none";
          window.scroll(0, 0);

          var fechaViajeIda = ""
          var fechaViajeVuelta = ""
          if (fecha_ida != "" && hora_ida != "") {
            fechaViajeIda = fecha_ida + " " + hora_ida + ":00"
          }
          if (fecha_vuelta != "" && hora_vuelta != "") {
            fechaViajeVuelta = fecha_vuelta + " " + hora_vuelta + ":00"
          }

          //Se setean datos adicionales
          this.obj_viaje["fechaViajeIda"] = fechaViajeIda
          this.obj_viaje["fechaIda"] = fecha_ida
          this.obj_viaje["horaIda"] = hora_ida
          this.obj_viaje["fechaViajeVuelta"] = fechaViajeVuelta
          this.obj_viaje["fechaVuelta"] = fecha_vuelta
          this.obj_viaje["horaVuelta"] = hora_vuelta
          this.obj_viaje["duracionViaje"] = duracion_viaje
          this.obj_viaje["duracionViajeFormateado"] = timeFormat
          this.obj_viaje["duracionViajeConTrafico"] = duracion_viaje_trafico

          if (hora_ida) {
            var date_obj = new Date();
            console.log(hora_ida.split(":")[0],hora_ida.split(":")[1])
            date_obj.setHours(hora_ida.split(":")[0]);
            date_obj.setMinutes(parseInt(hora_ida.split(":")[1]) + parseInt(duracion_viaje_trafico));
            console.log(date_obj)
            var horaLlegada = date_obj.getHours() + ":" + date_obj.getMinutes();
            this.obj_viaje["horaLlegada"] = horaLlegada;
          }
          var precio_total_viaje = 0;
          if (lugares_dePaso) {

            //Se calcula origen destino y distancia para cada ciudad de paso
            if (lugares_dePaso.length > 0) {
              var t30 = performance.now();
              var origen = inicio_viaje.lugar;
              var destino = ""
              var index = 0
              var distancia: any
              lugares_dePaso.forEach(element => {

                distancia = 0
                if (distanciasDividas[index]) {
                  distancia = distanciasDividas[index] / 1000
                  distancia = Math.ceil(distancia)
                }
                precio_total_viaje = precio_total_viaje + this.calcularCostoViajePorDistancia(distancia)
                destino = element.lugar;
                this.ciudadesDePaso.push({ "origen": origen, "destino": destino, "distancia": distancia })
                origen = element.lugar;
                index += 1
              });

              if (distanciasDividas[index]) {
                distancia = distanciasDividas[index] / 1000
                distancia = Math.ceil(distancia)
              }
              destino = fin_viaje.lugar;
              this.ciudadesDePaso.push({ "origen": origen, "destino": destino, "distancia": distancia })
              precio_total_viaje = precio_total_viaje + this.calcularCostoViajePorDistancia(distancia)

              //El precio total del viaje es la suma de los precios divididos
              this.obj_viaje["costoTotalViaje"] = precio_total_viaje

              //Disable si no tiene ciudades de paso

              var t60 = performance.now();
            } else {
              this.obj_viaje["costoTotalViaje"] = this.calcularCostoViajePorDistancia(this.obj_viaje["kilmetrosViaje"])
            }
          }
          localStorage.setItem("prueba", JSON.stringify(this.obj_viaje))

        } else {
          console.log("error en el formulario")
        }

        var t1 = performance.now();

      }
    );

  }

  hhmmToMinutes(time) {
    if (time != "") {
      var arrayTime = time.split(":");
      return parseInt(arrayTime[0]) * 60 + parseInt(arrayTime[1]);
    }
  }
  minutesTohhmm(minutes) {
    var num = parseInt(minutes);
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes: any = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + ":" + rminutes;
  }
  getAllDataAportacion() {
    var costo_unitario = (<any>document.getElementById(this.ciudadesDePaso.length + "_price")).value;
    var plazas_cant = (<any>document.getElementById("id_plaza_cant")).value;
    var equipajeSize = (<any>document.getElementsByClassName("selected-size")).length;
    var maxDosAtras = (<any>document.getElementById("check-3")).checked;
    var soloMujeres = (<any>document.getElementById("check-7")).checked;
    var solicitudTrayectoParcial = (<any>document.getElementById("check-2")).checked;
    var desvio = (<any>document.getElementById("id_desvio")).value;
    var datosAdicionales = (<any>document.getElementById("id_datosAdicionales")).value;
    this.obj_viaje["costo_unitario"] = costo_unitario
    this.obj_viaje["plazas_cant"] = plazas_cant
    this.obj_viaje["equipajeSize"] = equipajeSize
    this.obj_viaje["maxDosAtras"] = maxDosAtras
    this.obj_viaje["soloMujeres"] = soloMujeres
    this.obj_viaje["solicitudTrayectoParcial"] = solicitudTrayectoParcial
    this.obj_viaje["desvio"] = desvio
    this.obj_viaje["datosAdicionales"] = datosAdicionales
    this.obj_viaje["costoTotalViaje"] = (<any>document.getElementById(this.ciudadesDePaso.length + "_price")).value
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "block";
    window.scroll(0, 0);
  }

  changePrice($event) {
    var source = $event.target
    var id_element = $event.target.id.substr(0, 1)
    var precio_total = 0
    var precio_calculado = 0

    if (id_element == this.ciudadesDePaso.length) {

      precio_calculado = this.obj_viaje["costoTotalViaje"]
    } else {
      precio_calculado = this.calcularCostoViajePorDistancia(this.ciudadesDePaso[id_element].distancia)
    }

    var distanciasDividas = this.obj_viaje.distanciasDivididas
    var topeMaximo = this.calcularTopeMax(Math.ceil(distanciasDividas[id_element] / 1000))
    var price_invalid = (<any>document.getElementById("price_invalid"))

    if (source.value > precio_calculado && source.value <= topeMaximo) {
      $event.target.classList.remove("price-valid")
      $event.target.classList.add("price-invalid")
      price_invalid.innerText = "¡Trata de ingresar un monto en color verde para que consigas más reservas!"
      price_invalid.style.display = "block"
      this.recalcularPrecios()

    } else if (source.value > topeMaximo) {
      $event.target.value = precio_calculado
      $event.target.classList.add("price-valid")
      $event.target.classList.remove("price-invalid")
      price_invalid.innerText = "El valor ingresado supera el monto máximo del viaje por pasajero"
      price_invalid.style.display = "block"
      this.recalcularPrecios()
    } else {
      $event.target.classList.add("price-valid")
      $event.target.classList.remove("price-invalid")
      price_invalid.style.display = "none"
      this.recalcularPrecios()
    }
  }

  recalcularPrecios() {
    var totalCosto = 0
    var price_paso = (<any>document.getElementsByClassName("price-paso"))
    if (price_paso) {
      if (price_paso.length > 0) {
        price_paso.forEach(element => {
          console.log("costo", element.value)
          if (element.value != "" && !isNaN(element.value)) {
            totalCosto = totalCosto + parseInt(element.value)
          }
        });
        var totalPrecio = (<any>document.getElementById(this.ciudadesDePaso.length + "_price")).value = totalCosto
      }
    }
  }

  calcularTopeMax(distancia) {
    var costoIrSolo = this.calcularCostoViajePorDistancia(distancia)
    var topePrecio: any = costoIrSolo * 1.5
    topePrecio = parseInt(topePrecio)
    return topePrecio;
  }

  timeConvert(n) {
    var num = n;
    var hours = (num / 60);
    var rhours = Math.floor(hours);
    var minutes = (hours - rhours) * 60;
    var rminutes = Math.round(minutes);
    return rhours + " h " + rminutes + " min";
  }

  changeDiaHora() {
    var diahora = (<any>document.getElementById("check-1")).checked;
    var fechaFin = document.getElementById("fechaFin");

    if (diahora) {
      fechaFin.style.display = 'flex';
    } else {
      (<any>document.getElementById("id_fechaVuelta")).value = "";
      (<any>document.getElementById("id_horaVuelta")).value = "";
      fechaFin.style.display = 'none';
    }
  }

  setearDatosActuales() {
    if ((<any>this.viaje_obj).ciudadesPaso.length > 0) {
      var ciudadesDePaso = [...(<any>this.viaje_obj).ciudadesPaso]

    }
  }

  calcularCostoViajePorDistancia(distancia) {
    var costoIrSolo = (PRECIO_NAFTA * CONSUMO_PROM * distancia) / 100
    var costoUnitario: any = costoIrSolo / CANTIDAD_PASAJEROS_PROM
    costoUnitario = parseInt(costoUnitario)
    if (costoUnitario > 0) {
      if (Math.floor(costoUnitario / 1000) > 0) {
        if (costoUnitario % 1000 > 0) {
          costoUnitario = Math.floor(costoUnitario / 1000) * 1000 + 1000;
        }
      }
    }
    return costoUnitario
  }

  ngAfterViewInit() {
    this.mapInitializer();
  }

  constructor(private ngxService: NgxUiLoaderService,private toastr: ToastrService,private iterableDiffers: IterableDiffers, private router: Router, public registroService: RegistroService, public cocheService: CocheService) {
    this.markers.length = 3
    this.coordenadas.length = 3
    this.markers[0] = ""
    this.markers[1] = ""



  }
  confirmarReserva(bool) {
    this.calcularCo2Ahorrado();
    this.obj_viaje["aceptacionAutomatica"] = bool;
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "none";
    var resumen_section = document.getElementById("id_resumen_section").style.display = "block";
    window.scroll(0, 0);
  }

  redirectForm() {
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "block";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "none";
    window.scroll(0, 0);
  }
  redirectFormConfirmacion() {
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "block";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "none";
    window.scroll(0, 0);
  }
  redirectFormFromResumen() {
    var trayecto_section = document.getElementById("id_trayecto_section").style.display = "none";
    var trayecto_section = document.getElementById("id_aportacion_section").style.display = "none";
    var trayecto_section = document.getElementById("id_reserva_section").style.display = "block";
    var resumen_section = document.getElementById("id_resumen_section").style.display = "none";
    window.scroll(0, 0);
  }
  ConfirmarViaje() {
    var data = {
      "fechaViaje": this.obj_viaje["fechaViajeIda"],
      "cantidadAsientos": this.obj_viaje["plazas_cant"],
      "contribucionPorCabeza": this.obj_viaje["costo_unitario"],
      "idTamanhoEquipaje": this.obj_viaje["equipajeSize"],
      "lugarOrigen": this.obj_viaje["lugarOrigen"],
      "latitudOrigen": this.obj_viaje["latitudOrigen"],
      "longitudOrigen": this.obj_viaje["longitudOrigen"],
      "lugarDestino": this.obj_viaje["lugarDestino"],
      "latitudDestino": this.obj_viaje["latitudDestino"],
      "longitudDestino": this.obj_viaje["longitudDestino"],
      "idAuto": this.obj_viaje["idAuto"],
      "maximoDosAtras": this.obj_viaje["maxDosAtras"],
      "fechaVuelta": this.obj_viaje["fechaViajeVuelta"],
      "desvio": this.obj_viaje["desvio"],
      "solicitudTrayectoParcial": this.obj_viaje["solicitudTrayectoParcial"],
      "datosAdicionales": this.obj_viaje["datosAdicionales"],
      "aceptacionAutomatica": this.obj_viaje["aceptacionAutomatica"],
      "duracionViaje": this.obj_viaje["duracionViaje"],
      "distanciaTotal": this.obj_viaje["kilmetrosViaje"],
      "duracionViajeConTrafico": this.obj_viaje["duracionViajeConTrafico"],
      "ciudadesPaso": this.obj_viaje["ciudadesPaso"],
      "horaLlegada":this.obj_viaje["horaLlegada"],
      "soloMujeres":this.obj_viaje["soloMujeres"],
      "co2Ahorrado":this.obj_viaje["co2Ahorrado"]

    }
    this.registroService.registrarViaje(data).subscribe((response) => {
      if (response.estado == 0) {
        this.router.navigate(["/publicar/confirmacion"])
      }else{
        this.toastr.error("Publicar viaje:" + response.mensaje);
      }
    })
  }

  calcularCo2Ahorrado() {
    var distancia = this.obj_viaje["kilmetrosViaje"]
    this.emisionCo2 = distancia * CONSUMO_PROM * EMISION_ESTANDAR_CO2 / 100
    this.emisionCo2 = Math.round(this.emisionCo2);
    return this.emisionCo2;
  }
  
  selectSize(size) {
    var sizes = (<any>document.getElementsByClassName("sizes"))
    if (size == 1) {
      if (!sizes[0].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
      }
    } else if (size == 2) {
      if (!sizes[1].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
      } else if (!sizes[2].classList.contains("selected-size")) {
        sizes[1].classList.remove("selected-size");
      }
    } else if (size == 3) {
      if (!sizes[2].classList.contains("selected-size")) {
        sizes[0].classList.add("selected-size");
        sizes[1].classList.add("selected-size");
        sizes[2].classList.add("selected-size");
      } else {
        sizes[2].classList.remove("selected-size");
      }
    }
  }
  getCoche() {
    this.cocheService.listarCochesActivos().subscribe(response => {
      if (response.estado == 0) {
        this.obj_viaje["idAuto"] = response.data[0].id;
      }else{
        this.toastr.error("Obtener coches: " + response.mensaje);
      }
    });
  }
  ngOnInit() {
    this.getCoche();
    var currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    this.onlyMujeres = (currentUser.sexo != "MASCULINO");
  }

}
