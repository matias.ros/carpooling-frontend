import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicarConfirmacionComponent } from './publicar-confirmacion.component';

describe('PublicarConfirmacionComponent', () => {
  let component: PublicarConfirmacionComponent;
  let fixture: ComponentFixture<PublicarConfirmacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicarConfirmacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicarConfirmacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
