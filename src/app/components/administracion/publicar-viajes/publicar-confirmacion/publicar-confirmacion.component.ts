import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publicar-confirmacion',
  templateUrl: './publicar-confirmacion.component.html',
  styleUrls: ['./publicar-confirmacion.component.scss']
})
  
export class PublicarConfirmacionComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  empezar(){
    this.router.navigate([""]);
  }
}
