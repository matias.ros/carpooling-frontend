import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservaAutomaticaComponent } from './reserva-automatica.component';

describe('ReservaAutomaticaComponent', () => {
  let component: ReservaAutomaticaComponent;
  let fixture: ComponentFixture<ReservaAutomaticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservaAutomaticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservaAutomaticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
