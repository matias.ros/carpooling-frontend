import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificarTelefonoComponent } from './certificar-telefono.component';

describe('CertificarTelefonoComponent', () => {
  let component: CertificarTelefonoComponent;
  let fixture: ComponentFixture<CertificarTelefonoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificarTelefonoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificarTelefonoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
