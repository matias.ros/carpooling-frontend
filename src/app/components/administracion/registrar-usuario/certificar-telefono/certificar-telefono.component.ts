import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Router } from "@angular/router";
import { RegistroService } from "src/app/services/registro.service";
import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-certificar-telefono",
  templateUrl: "./certificar-telefono.component.html",
  styleUrls: ["./certificar-telefono.component.scss"]
})
  
export class CertificarTelefonoComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  showInputCodigo = false;
  showInputNumero = true;
  showExito = false;
  codigoIngresado = "";
  phoneNumber: any;
  dataRegistry: any = {};
  /* PARA EL INPUT CON CODIGOS */
  separateDialCode = true;
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.Paraguay,
    CountryISO.Brazil,
    CountryISO.Argentina,
    CountryISO.UnitedStates
  ];
  phoneForm = new FormGroup({
    phone: new FormControl(undefined, [
      Validators.required,
      Validators.maxLength(10),
      Validators.minLength(9)
    ])
  });

  /* PARA EL INPUT CON CODIGOS */

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public registroService: RegistroService,
    private toastr: ToastrService
  ) {
    this.registerForm = this.formBuilder.group({
      telefono: [
        "",
        [Validators.required, Validators.maxLength(20), Validators.minLength(9)]
      ],
      acceptTerms: [false]
    });
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"));
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    var tel_mensaje = <any>document.getElementById("tel_invalid");
    this.submitted = true;

    if (this.phoneForm.invalid) {
      tel_mensaje.style.display = "block";
      return;
    }
    if (this.dataRegistry["telefono"] != "") {
      this.dataRegistry[
        "telefono"
      ] = this.phoneForm.value.phone.internationalNumber.replace(/\s/g, "");
      this.enviarMensaje();
    }
  }

  enviarMensaje() {
    var data = this.dataRegistry["telefono"];
    this.registroService.enviarSMS(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.showInputNumero = false;
          this.showInputCodigo = true;
          console.log(response);
        }
      }
    });
  }

  verificarCodigo() {
    var data = this.codigoIngresado;
    this.registroService.verificacionCodigo(data).subscribe(response => {
      if (response) {
        if (response.estado == 0) {
          this.showInputCodigo = false;
          this.showExito = true;
          console.log(response);
        } else {
          var email_mensaje = <any>document.getElementById("codigo_invalid");
          email_mensaje.style.display = "block";
        }
      }
    });
    console.log(this.codigoIngresado);
  }

  empezar() {
    this.router.navigate([""]);
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  ngOnInit() {}
}
