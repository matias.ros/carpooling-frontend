import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresarContraseniaComponent } from './ingresar-contrasenia.component';

describe('IngresarContraseniaComponent', () => {
  let component: IngresarContraseniaComponent;
  let fixture: ComponentFixture<IngresarContraseniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresarContraseniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresarContraseniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
