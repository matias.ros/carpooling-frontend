import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PasswordValidator } from '../../../../validators/password.validator';
import { RegistroService } from 'src/app/services/registro.service';
import { OauthService } from 'src/app/services/oauth.service';

@Component({
  selector: 'app-ingresar-contrasenia',
  templateUrl: './ingresar-contrasenia.component.html',
  styleUrls: ['./ingresar-contrasenia.component.scss']
})
  
export class IngresarContraseniaComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  dataRegistry: any = {}
  passwordStatus = 0;
  passwordMessage = 'La contraseña es requerida';
  progressType = 'danger';

  constructor(public oauthService: OauthService,private formBuilder: FormBuilder, private router: Router, public registroService: RegistroService, ) {
    this.registerForm = this.formBuilder.group({
      password: ['', [Validators.required]]
    }
      //En el caso de querer validar coincidencia de contraseñas
      // , 
      // {
      //   validator: MustMatch('password', 'confirmPassword')
      // }
    );

    //Se obtiene datos del flujo registro
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"))
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.dataRegistry["password"] = this.registerForm.value.password
    sessionStorage.setItem("dataRegistry", JSON.stringify(this.dataRegistry))
    this.registrarUsuario()
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }

  onChanges(): void {
    var indexaroba = this.dataRegistry.email.indexOf("@")
    var email = this.dataRegistry.email.substr(0, indexaroba)
    this.f.password.valueChanges.subscribe(val => {
      let validator = PasswordValidator.validator(val, email);
      this.passwordStatus = 20 + validator.rate * 20;
      this.passwordMessage = validator.message;
      this.progressType = validator.type;
    });


  }
  registrarUsuario() {
    var data = this.dataRegistry;
    this.registroService.registrarUsuario(data).subscribe((response) => {
      if (response) {
        if (response.estado == 0) {
          this.loginUser();
        }
      }

    })
  }
  loginUser() {
    var data  = {
      "user":this.dataRegistry.email,
      "password":this.dataRegistry.password,
      "isFbLogin":false
    }
    this.oauthService.loginOauth(data).subscribe((response) => {
      if (response.estado == 0) {
        sessionStorage.setItem("loggedIn", 'true');
        sessionStorage.setItem("currentUser", JSON.stringify(response.data));
        this.router.navigate(['registro/telefono'])
      } else if (response.code && response.code === 302) {
        //this.router.navigate(['/seguridad/crear-usuario', { comprobarFB: true, email: this.user }]);
      } else {
        // this.showAlert = true;
        // this.mensajeAlert = response.message;
      }
    })
  }
  ngOnInit() {
    this.onChanges();
  }

}
