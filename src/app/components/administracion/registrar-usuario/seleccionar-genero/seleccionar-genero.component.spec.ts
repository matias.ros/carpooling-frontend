import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarGeneroComponent } from './seleccionar-genero.component';

describe('SeleccionarGeneroComponent', () => {
  let component: SeleccionarGeneroComponent;
  let fixture: ComponentFixture<SeleccionarGeneroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarGeneroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarGeneroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
