import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seleccionar-genero',
  templateUrl: './seleccionar-genero.component.html',
  styleUrls: ['./seleccionar-genero.component.scss']
})
export class SeleccionarGeneroComponent implements OnInit {

  dataRegistry:any={};

  seleccionaGenero(genero){
    console.log(genero)
    this.dataRegistry["sexo"] = genero
    sessionStorage.setItem("dataRegistry",JSON.stringify(this.dataRegistry))
    this.router.navigate(['registro/pass'])
  }

  constructor(private router: Router) {
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"))
   }

  ngOnInit() {
  }

}
