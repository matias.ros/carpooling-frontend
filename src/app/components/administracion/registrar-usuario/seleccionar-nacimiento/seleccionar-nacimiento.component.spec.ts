import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarNacimientoComponent } from './seleccionar-nacimiento.component';

describe('SeleccionarNacimientoComponent', () => {
  let component: SeleccionarNacimientoComponent;
  let fixture: ComponentFixture<SeleccionarNacimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarNacimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarNacimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
