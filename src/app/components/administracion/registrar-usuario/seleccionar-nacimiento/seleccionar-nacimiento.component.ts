import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { $ } from 'src/assets/js/jquery.min.js';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
@Component({
  selector: 'app-seleccionar-nacimiento',
  templateUrl: './seleccionar-nacimiento.component.html',
  styleUrls: ['./seleccionar-nacimiento.component.scss']
})
 
export class SeleccionarNacimientoComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  dataRegistry: any = {}
  @Input() name: string = '';
  @Input() model: any;
  @Input() placeholder: string = 'DD/MM/YYYY';
  @Input() type: string = 'calendar'; 
  @Output() dateChange = new EventEmitter();
  private propagateChange:any = () => {};

  constructor(private formBuilder: FormBuilder, private router: Router, private dateService:DateService) {

    this.registerForm = this.formBuilder.group({
      fechaNacimiento: ['', [Validators.required]]
    }
      //En el caso de querer validar coincidencia de contraseñas
      // , 
      // {
      //   validator: MustMatch('password', 'confirmPassword')
      // }
    ); 

    //Se obtiene datos del flujo registro
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"))
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.dataRegistry["fechaNacimiento"] = this.dateService.getOnlyDate(this.registerForm.value.fechaNacimiento)
    sessionStorage.setItem("dataRegistry", JSON.stringify(this.dataRegistry))
    this.router.navigate(['registro/genero'])
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  ngOnInit() {
  }

}
