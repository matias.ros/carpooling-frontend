import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarNombreComponent } from './seleccionar-nombre.component';

describe('SeleccionarNombreComponent', () => {
  let component: SeleccionarNombreComponent;
  let fixture: ComponentFixture<SeleccionarNombreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarNombreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
