import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seleccionar-nombre',
  templateUrl: './seleccionar-nombre.component.html',
  styleUrls: ['./seleccionar-nombre.component.scss']
})
export class SeleccionarNombreComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  dataRegistry: any = {}
  constructor(private formBuilder: FormBuilder, private router: Router) {
    
    this.registerForm = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
    }
      //En el caso de querer validar coincidencia de contraseñas
      // , 
      // {
      //   validator: MustMatch('password', 'confirmPassword')
      // }
    );

    //Se obtiene datos del flujo registro
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"))
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.dataRegistry["nombre"] = this.registerForm.value.nombre
    this.dataRegistry["apellido"] = this.registerForm.value.apellido
    sessionStorage.setItem("dataRegistry", JSON.stringify(this.dataRegistry))
    this.router.navigate(['registro/nacimiento'])
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  ngOnInit() {
  }


}
