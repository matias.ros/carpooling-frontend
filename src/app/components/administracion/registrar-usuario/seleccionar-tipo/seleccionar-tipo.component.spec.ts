import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarTipoComponent } from './seleccionar-tipo.component';

describe('SeleccionarTipoComponent', () => {
  let component: SeleccionarTipoComponent;
  let fixture: ComponentFixture<SeleccionarTipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarTipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
