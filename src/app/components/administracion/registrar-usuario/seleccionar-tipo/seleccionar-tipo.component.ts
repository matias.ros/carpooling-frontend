import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookService, LoginOptions, LoginResponse } from 'ngx-facebook';
import { RegistroService } from 'src/app/services/registro.service';

@Component({
  selector: 'app-seleccionar-tipo',
  templateUrl: './seleccionar-tipo.component.html',
  styleUrls: ['./seleccionar-tipo.component.scss']
})
export class SeleccionarTipoComponent implements OnInit {
  dataRegistry:any={};
  
  seleccionarTipo(tipoId){

    this.dataRegistry["isFacebookRegistry"] =tipoId
    sessionStorage.setItem("dataRegistry",JSON.stringify(this.dataRegistry))
    if(!tipoId){
      this.router.navigate(['registro/email'])
    }else{
      //Es por facebook
      this.checkFbStatus();
    }

    
  }

  checkFbStatus() {
    this.fb.getLoginStatus().then((response) => {
      console.log("checkFbStatus", response)
      if (response.authResponse && response.status === "connected") {
        console.log("Ya estoy conectado, ",response.authResponse.accessToken);
        this.registrarUsuario({"token":response.authResponse.accessToken,"isFacebookRegistry":true})
      }
      else {
        this.loginFB();
      }
    }).catch((error: any) => {
      // this.showAlert = true;
      // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
    });
  }
  loginFB() {
    console.log("loginFB")
    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: 'user_birthday,user_photos,email,public_profile,user_gender',
      auth_type: 'rerequest'
    } 
    this.fb.login(loginOptions)
      .then((response: LoginResponse) => {
        if (response.authResponse && response.status === "connected") {
          console.log("fb.login",response)
          console.log("ahora si estoy logueado", response.authResponse.accessToken)
          this.registrarUsuario({"token":response.authResponse.accessToken,"isFacebookRegistry":true})
        }
        else {
          // this.showAlert = true;
          // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
        }
      })
      .catch((error: any) => {
        // this.showAlert = true;
        // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
      });

  }

  registrarUsuario(data){
    this.registroService.registrarUsuario(data).subscribe((response) => {
      console.log(response)
      if(response){
        if(response.estado==0){
          this.router.navigate(['registro/telefono'])
        }
      }
      
    })
  }
  constructor(private router: Router,private fb: FacebookService, private registroService: RegistroService) { }

  ngOnInit() {
    sessionStorage.removeItem("dataRegistry")
  }

}
