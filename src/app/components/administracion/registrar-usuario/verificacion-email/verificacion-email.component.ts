import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MustMatch } from "src/app/validators/mustmatch";
import { Router } from "@angular/router";
import { RegistroService } from "src/app/services/registro.service";

MustMatch;
@Component({
  selector: "app-verificacion-email",
  templateUrl: "./verificacion-email.component.html",
  styleUrls: ["./verificacion-email.component.scss"]
})
export class VerificacionEmailComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  dataRegistry: any = {};
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public registroService: RegistroService
  ) {
    this.registerForm = this.formBuilder.group(
      {
        email: ["", [Validators.required, Validators.email]],
        acceptTerms: [false]
      }
      //En el caso de querer validar coincidencia de contraseñas
      // ,
      // {
      //   validator: MustMatch('password', 'confirmPassword')
      // }
    );

    //Se obtiene datos del flujo registro
    this.dataRegistry = JSON.parse(sessionStorage.getItem("dataRegistry"));
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.validarCorreo();
  }

  validarCorreo() {
    var data = this.registerForm.value.email;
    var email_mensaje = <any>document.getElementById("email_invalid");
    this.registroService.existenciaCorreo(data).subscribe(response => {
      if (response) {
        console.log("VALIDAAR CORREO", response);
        if (response.estado == 0) {
          if (response.data[0].existe) {
            email_mensaje.style.display = "block";
            console.log("YA EXISTE EL CORREO!");
          } else {
            email_mensaje.style.display = "none";
            this.dataRegistry["email"] = this.registerForm.value.email;
            this.dataRegistry[
              "aceptaNotificaciones"
            ] = this.registerForm.value.acceptTerms;
            sessionStorage.setItem(
              "dataRegistry",
              JSON.stringify(this.dataRegistry)
            );
            this.router.navigate(["registro/nombre"]);
          }
        }
      }
    });
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  ngOnInit() {}
}
