import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { OauthService } from "src/app/services/oauth.service";
import { CocheService } from "src/app/services/coche.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  user: any;
  @Input() public isUserLoggedIn: boolean;
  @Input() public nombreuser: string;
  @Input() public apellidouser: string;
  @Input() public fotouserFB: string;
  @Input() public style_header: boolean;
  constructor(
    public router: Router,
    public oauthService: OauthService,
    public cocheService: CocheService
  ) {}

  ngOnInit() {
    var bool: any = this.isUserLoggedIn;
    this.isUserLoggedIn = bool == "true";
    console.log("bool", this.isUserLoggedIn);
  }

  logout() {
    this.oauthService.logout().subscribe(response => {
      if (response.estado == 0) {
        sessionStorage.removeItem("loggedIn");
        //window.location.reload();
        this.router.navigate(["/login"]);
      } else if (response.code && response.code === 302) {
        //this.router.navigate(['/seguridad/crear-usuario', { comprobarFB: true, email: this.user }]);
      } else {
        // this.showAlert = true;
        // this.mensajeAlert = response.message;
      }
    });
  }

  buscarViajes(){
    sessionStorage.removeItem("obj_filtro");
    this.router.navigate(["/buscar"]);
  }
  verificarCoche() {
    this.cocheService.listarCochesActivos().subscribe(response => {
      if (response.estado == 0) {
        if (response.data.length == 0) {
          var modalAlert = document.getElementById("moda-alert").click();
        } else if (response.data.length > 0) {
          this.router.navigate(["/publicar"]);
        }
        console.log(response);
      }
    });
  }
}
