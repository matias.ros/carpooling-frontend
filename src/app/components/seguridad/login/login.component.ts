import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OauthService } from "src/app/services/oauth.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FacebookService, LoginResponse, LoginOptions } from "ngx-facebook";
import { ToastrService } from "ngx-toastr";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  email: string = null;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    public oauthService: OauthService,
    private fb: FacebookService,
    private toastr: ToastrService,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      user: ["", Validators.required],
      password: ["", Validators.required]
    });    //this.showSuccess("Success");
    //this.showError("Error");

    var clicked = false;
    var submit = document.getElementsByClassName('submit')[0];
    submit.addEventListener("click", function () {
      // Make sure user cannot click button again until it has been reset
      if (!clicked) {
        clicked = true;
        submit.classList.remove("return");
        (<any>submit).blur();
        document.querySelector('.loading-dock').classList.add('loaded');
        document.getElementById('load').style.display = 'initial';
        document.getElementById('load-b').style.display = 'initial';
        setTimeout(function () {
          (<any>document.getElementById('load')).style.opacity = 1;
        }, 750);
        setTimeout(function () {
          (<any>document.getElementById('load-b')).style.opacity = 1;
        }, 900);

        //reset all
        setTimeout(function () {
          submit.classList.remove("popout");
          submit.classList.add("return");
          submit.innerHTML = "Iniciar Sesión";
          if(document.getElementById('check')){
            document.getElementById('check').style.display = "none";
          }
          if(document.getElementById('times')){
            document.getElementById('times').style.display = "none";
          }
          
          clicked = false;
        }, 5000);
      }
    })
    // this.oauthService.logout();
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  get f() {
    return this.loginForm.controls;
  }

  loginOauth(fbParams) {
    //let { token, email } = fbParams ? fbParams : { token: null, email: null };
    let { token } = fbParams ? fbParams : { token: null };
    let data = {
      token,
      // email,
      user: "",
      password: "",
      isFbLogin: false
    };
    if (!fbParams) {
      this.submitted = true;

      if (this.loginForm.invalid) {
        return;
      }
      data.user = this.f.user.value;
      data.password = this.f.password.value;
    } else {
      data.isFbLogin = true;
    }
    console.log(data);
    this.oauthService.loginOauth(data).subscribe(response => {
      if (response.estado == 0) {
        this.endLoaderSuccess(response.data)
      } else if (response.code && response.code === 302) {
        this.endLoaderError(response.mensaje)
      } else {
        this.endLoaderError(response.mensaje)
      }
    });
  }
  endLoaderError(mensaje) {
    setTimeout(() => {
      document.querySelector('.loading-dock').classList.remove('loaded');
      document.getElementById('load').style.display = 'none';
      document.getElementById('load-b').style.display = 'none';
      (<any>document.getElementById('load')).style.opacity = 0;
      (<any>document.getElementById('load-b')).style.opacity = 0;
      let submit = document.querySelector('.submit');
      submit.classList.add("popout");
      submit.innerHTML = "";
      setTimeout(function () {
        document.getElementById('times').style.display = "block";
      }, 300);
      this.showError(mensaje)
    }, 1000);
  }
  endLoaderSuccess(data) {
    setTimeout(() => {
      document.querySelector('.loading-dock').classList.remove('loaded');
      document.getElementById('load').style.display = 'none';
      document.getElementById('load-b').style.display = 'none';
      (<any>document.getElementById('load')).style.opacity = 0;
      (<any>document.getElementById('load-b')).style.opacity = 0;
      let submit = document.querySelector('.submit');
      submit.classList.add("popout");
      submit.innerHTML = "";
      sessionStorage.setItem("loggedIn", "true");
      sessionStorage.setItem("currentUser", JSON.stringify(data));
      setTimeout(() => {
        document.getElementById('check').style.display = "block";

      }, 300);
      setTimeout(() => {
        this.router.navigate([""]);
      }, 1000);

    }, 1000);
  }
  checkFbStatus() {
    this.fb
      .getLoginStatus()
      .then(response => {
        console.log("checkFbStatus", response);
        if (response.authResponse && response.status === "connected") {
          this.getFBData(response.authResponse.accessToken);
        } else {
          this.loginFB();
        }
      })
      .catch((error: any) => {
        // this.showAlert = true;
        // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
      });
  }
  showSuccess(mensaje) {
    this.toastr.success("Correcto", mensaje);
  }
  showError(mensaje) {
    this.toastr.error(mensaje, "Error");
  }

  loginFB() {
    const loginOptions: LoginOptions = {
      enable_profile_selector: true,
      return_scopes: true,
      scope: "user_birthday,user_photos,email,public_profile",
      auth_type: "rerequest"
    };
    this.fb
      .login(loginOptions)
      .then((response: LoginResponse) => {
        if (response.authResponse && response.status === "connected") {
          console.log("fb.login", response);
          this.getFBData(response.authResponse.accessToken);
        } else {
          // this.showAlert = true;
          // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
        }
      })
      .catch((error: any) => {
        // this.showAlert = true;
        // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
      });
  }
  getFBData(token) {
    this.fb
      .api("/me", "get", { fields: "email" })
      .then(response => {
        console.log("getFBData", response);
        this.email = response.email;
        // Oauth2.loginOauth(accessToken, email);
        this.loginOauth({ token, email: this.email });
      })
      .catch((error: any) => {
        // this.showAlert = true;
        // this.mensajeAlert = 'Ocurrió un problema al iniciar sesión con Facebook'
      });
  }
}
