import {environment} from '../../environments/environment';


const url = '/carpooling-web/api/';

const oauth2BaseUrl = '/sesion/login';


const Servers = {
  Carpooling: {
    baseUrl: `${url}`
  },
  Auth: {
    baseUrl: `${oauth2BaseUrl}`
  }
};
const Conf = {
  OAuth2: {
    RedirectUri: {
      Default: '',
      PortalMovil: ''
    },
    ResponseTypes: {
      Token: 'token'
    },
    GrantTypes: {
      Implicit: 'implicit',
      Facebook: 'facebook',
      ClientCredentials: 'client_credentials'
    }
  },
  timeout: 25000
};

export { Servers,Conf };
