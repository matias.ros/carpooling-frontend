import { environment } from "../../environments/environment";

export const OPERACION_ACTIVAR = "ACT";

export const OPERACION_SUSCRIBIR = "SUS";

export const OPERACION_REACTIVAR = "REA";

export const FACEBOOK_APP_ID = environment.production
  ? "573160280211178"
  : "573160280211178";

export const TOOLTIPS = {
  crearCuenta: "/seguridad/crear-usuario/",
  recuperarPassword: "/seguridad/recu-auto-contrasena/",
  resetearPassword: "/administracion/resetear-contrasena/",
  editarPermisos: "/administracion/nucleo/editar-permisos/",
  gestionUsuarios: "/administracion/gestion-usuarios/",
  agregarUsuario: "/administracion/nucleo/agregar-usuarios/",
  configuraciones: "/administracion/configuraciones/",
  colas: "/administracion/colas/"
};

export const ANALYTICS_KEY = "UA-43824803-12";

export const cleanParams = function(obj) {
  for (let propName in obj) {
    if (
      obj[propName] === null ||
      obj[propName] === undefined ||
      obj[propName] === ""
    ) {
      delete obj[propName];
    }
  }
  console.log(obj);
  return obj;
};

export const quitarCero = function(linea) {
  if (linea[0] === "0" && linea[1] === "9") {
    return linea.slice(1);
  } else {
    return linea;
  }
};

export const permisosVistas = {
  "/administracion/modificar-permisos": ["USR_NUC", "ADM_USUARIOS"],
  "/administracion/agregar-usuario": ["USR_NUC", "ADM_USUARIOS"],
  "/administracion/gestion-usuarios": ["ADM_USUARIOS"],
  "/administracion/inject": ["USR_NUC", "ADM_USUARIOS", "ITS_LOGGED_IN"],
  "/administracion/detalle-columnas": [
    "USR_NUC",
    "ADM_USUARIOS",
    "ITS_LOGGED_IN"
  ],
  "/administracion/configuraciones": [
    "USR_NUC",
    "ADM_USUARIOS",
    "ITS_LOGGED_IN"
  ],
  "/administracion/colas": ["USR_NUC", "ADM_USUARIOS", "ITS_LOGGED_IN"]
};

export const HEADERWHITE = ["registro", "mapa", "publicar", "perfil","buscar","solicitudes","viajespublicados"];

//Viaje precio aportacion por pasajero
export const PRECIO_NAFTA = 6700
export const CONSUMO_PROM = 12
export const EMISION_ESTANDAR_CO2 = 2.39
export const CANTIDAD_PASAJEROS_PROM = 4

export const permisosDefectoLineas = [
  "CONS_DATOS_LINEA",
  "CONS_GRUPO_FACT",
  "CONS_FACTURAS",
  "CONS_FINANCIACIONES",
  "CONS_FORMS_DOCS",
  "BACKUP_AGENDA",
  "MODIF_DATOS_USUARIO",
  "ACTIV_LINEA_NUEVA",
  "MODIF_TERMINAL",
  "MODIF_BACKTONE",
  "ACTIV_DEST_GRATIS",
  "ACC_REC_TARJETA",
  "PEDI_SALDO",
  "PRESTA_SALDO",
  "RECARGA_FACT_OP",
  "ACTIV_PACK"
];

export const colores = [
	{
		color: "Negro",
		codigo: "#000000",
		borde: "#000000"
	},
	{
		color: "Blanco",
		codigo: "#FFFFFF",
		borde: "lightgrey"
	},
	{
		color: "Dorado",
		codigo: "#D4AF37",
		borde: "#D4AF37"
	},
	{
		color: "Rojo",
		codigo: "#db1818",
		borde: "#db1818"
	},
	{
		color: "Gris Oscuro",
		codigo: "#757575",
		borde: "#757575"
	},
	{
		color: "Gris",
		codigo: "#c7c7c7",
		borde: "#c7c7c7"
	},
	{
		color: "Azul Oscuro",
		codigo: "#2a2b57",
		borde: "#2a2b57"
	},
	{
		color: "Azul",
		codigo: "#2226b5",
		borde: "#2226b5"
	},
	{
		color: "Naranja",
		codigo: "#cf740c",
		borde: "#cf740c"
	},
	{
		color: "Marrón",
		codigo: "#5c471e",
		borde: "#5c471e"
	},
	{
		color: "Beige",
		codigo: "#c4a260",
		borde: "#c4a260"
	}
]
export const marcasFrecuentes = [
  {
    "name": "KIA"
  }, {
    "name": "TOYOTA"
  }, {
    "name": "CHEVROLET"
  }, {
    "name": "NISSAN"
  }
];

export const listaMarcas = [{
	"name": "AC"
}, {
	"name": "AC PROPULSION"
}, {
	"name": "ACURA"
}, {
	"name": "A.D. TRAMONTANA"
}, {
	"name": "ALFA ROMEO"
}, {
	"name": "ALMAC"
}, {
	"name": "ALTERNATIVE CARS"
}, {
	"name": "AMUZA"
}, {
	"name": "ANTEROS"
}, {
	"name": "ARASH"
}, {
	"name": "ARIEL"
}, {
	"name": "ARRINERA"
}, {
	"name": "ASL"
}, {
	"name": "ASTERIO"
}, {
	"name": "ASTON MARTIN"
}, {
	"name": "AUDI"
}, {
	"name": "BAC"
}, {
	"name": "BAJAJ"
}, {
	"name": "BEIJING AUTOMOBILE WORKS"
}, {
	"name": "BENTLEY"
}, {
	"name": "BMW"
}, {
	"name": "BOLLORÉ"
}, {
	"name": "BOLWELL"
}, {
	"name": "BRILLIANCE / HUACHEN"
}, {
	"name": "BRISTOL"
}, {
	"name": "BRITISH LEYLAND"
}, {
	"name": "BRM BUGGY"
}, {
	"name": "BROOKE"
}, {
	"name": "BUDDY"
}, {
	"name": "BUFORI"
}, {
	"name": "BUGATTI"
}, {
	"name": "BUICK"
}, {
	"name": "BYD"
}, {
	"name": "CADILLAC"
}, {
	"name": "CAPARO"
}, {
	"name": "CARBONTECH"
}, {
	"name": "CARICE"
}, {
	"name": "CHANG'AN"
}, {
	"name": "CHANGHE"
}, {
	"name": "CHERY"
}, {
	"name": "CHEVROLET"
}, {
	"name": "CHEVRON"
}, {
	"name": "CITROËN"
}, {
	"name": "CHRYSLER"
}, {
	"name": "COMMUTER CARS"
}, {
	"name": "CONNAUGHT"
}, {
	"name": "COVINI"
}, {
	"name": "DACIA"
}, {
	"name": "DAIHATSU"
}, {
	"name": "DATSUN"
}, {
	"name": "DE LA CHAPELLE"
}, {
	"name": "DMC"
}, {
	"name": "DIARDI"
}, {
	"name": "DODGE"
}, {
	"name": "DONKERVOORT"
}, {
	"name": "DONGFENG"
}, {
	"name": "DONTO"
}, {
	"name": "DS AUTOMOBILES"
}, {
	"name": "DYNASTI ELECTRIC CAR CORP."
}, {
	"name": "E-VADE"
}, {
	"name": "EFFEDI"
}, {
	"name": "EGY-TECH ENGINEERING"
}, {
	"name": "ELECTRIC RACEABOUT"
}, {
	"name": "ELFIN"
}, {
	"name": "EMGRAND"
}, {
	"name": "ENGLON"
}, {
	"name": "ETERNITI"
}, {
	"name": "ETOX"
}, {
	"name": "EQUUS"
}, {
	"name": "EXAGON"
}, {
	"name": "FARALLI & MAZZANTI"
}, {
	"name": "FAW"
}, {
	"name": "FERRARI"
}, {
	"name": "FIAT"
}, {
	"name": "FISKER"
}, {
	"name": "FODAY"
}, {
	"name": "FORCE"
}, {
	"name": "FORD"
}, {
	"name": "FORD AUSTRALIA"
}, {
	"name": "FORD GERMANY"
}, {
	"name": "FORNASARI"
}, {
	"name": "FRASER"
}, {
	"name": "GAC GROUP"
}, {
	"name": "GALPIN"
}, {
	"name": "GEELY"
}, {
	"name": "GENESIS"
}, {
	"name": "GIBBS"
}, {
	"name": "GILLET"
}, {
	"name": "GINETTA"
}, {
	"name": "GMC"
}, {
	"name": "GONOW"
}, {
	"name": "GREAT WALL / CHANGCHENG"
}, {
	"name": "GREENTECH AUTOMOTIVE"
}, {
	"name": "GRINNALL"
}, {
	"name": "GTA MOTOR"
}, {
	"name": "GUMPERT"
}, {
	"name": "GURGEL"
}, {
	"name": "HENNESSEY"
}, {
	"name": "HINDUSTAN"
}, {
	"name": "HOLDEN"
}, {
	"name": "HONDA"
}, {
	"name": "HONGQI"
}, {
	"name": "HRADYESH"
}, {
	"name": "HTT TECHNOLOGIES"
}, {
	"name": "HULME"
}, {
	"name": "HYUNDAI"
}, {
	"name": "ICML"
}, {
	"name": "IFR"
}, {
	"name": "IRAN KHODRO"
}, {
	"name": "IKCO"
}, {
	"name": "IMPERIA"
}, {
	"name": "INFINITI"
}, {
	"name": "IVM"
}, {
	"name": "INVICTA"
}, {
	"name": "ISDERA"
}, {
	"name": "ISUZU"
}, {
	"name": "JAC"
}, {
	"name": "JAGUAR"
}, {
	"name": "JEEP"
}, {
	"name": "JENSEN MOTORS"
}, {
	"name": "JETCAR"
}, {
	"name": "JONWAY"
}, {
	"name": "JOSS"
}, {
	"name": "KAIPAN"
}, {
	"name": "KANTANKA"
}, {
	"name": "KARMA"
}, {
	"name": "KOENIGSEGG"
}, {
	"name": "KORRES"
}, {
	"name": "KIA"
}, {
	"name": "KIAT"
}, {
	"name": "KISH KHODRO"
}, {
	"name": "KTM"
}, {
	"name": "LADA"
}, {
	"name": "LAMBORGHINI"
}, {
	"name": "LANCIA"
}, {
	"name": "LAND ROVER"
}, {
	"name": "LANDWIND"
}, {
	"name": "LARAKI"
}, {
	"name": "LEBLANC"
}, {
	"name": "LEITCH"
}, {
	"name": "LEOPARD"
}, {
	"name": "LEXUS"
}, {
	"name": "LI-ION"
}, {
	"name": "LIFAN"
}, {
	"name": "LIGHTNING"
}, {
	"name": "LINCOLN"
}, {
	"name": "LISTER"
}, {
	"name": "LOCAL MOTORS"
}, {
	"name": "LOBINI"
}, {
	"name": "LOTEC"
}, {
	"name": "LOTUS CARS"
}, {
	"name": "LUCRA CARS"
}, {
	"name": "LUXGEN"
}, {
	"name": "MAHINDRA"
}, {
	"name": "MARUSSIA"
}, {
	"name": "MARUTI SUZUKI"
}, {
	"name": "MASERATI"
}, {
	"name": "MASTRETTA"
}, {
	"name": "MAZDA"
}, {
	"name": "MCLAREN"
}, {
	"name": "MERCEDES-BENZ"
}, {
	"name": "MG"
}, {
	"name": "MICRO"
}, {
	"name": "MINI"
}, {
	"name": "MITSUBISHI"
}, {
	"name": "MITSUOKA"
}, {
	"name": "MORGAN"
}, {
	"name": "MULLEN"
}, {
	"name": "MYCAR"
}, {
	"name": "MYVI-PERODUA"
}, {
	"name": "NISSAN"
}, {
	"name": "NOBLE"
}, {
	"name": "NOTA"
}, {
	"name": "OLDSMOBILE"
}, {
	"name": "OPEL"
}, {
	"name": "OPTIMAL ENERGY"
}, {
	"name": "ORCA"
}, {
	"name": "OLTCIT"
}, {
	"name": "PAGANI"
}, {
	"name": "PANHARD"
}, {
	"name": "PANOZ"
}, {
	"name": "PERANA"
}, {
	"name": "PERODUA"
}, {
	"name": "PEUGEOT"
}, {
	"name": "P.G.O."
}, {
	"name": "POLARSUN"
}, {
	"name": "PLYMOUTH"
}, {
	"name": "PORSCHE"
}, {
	"name": "PROTO"
}, {
	"name": "OULLIM"
}, {
	"name": "PROTON"
}, {
	"name": "PURITALIA"
}, {
	"name": "QOROS"
}, {
	"name": "QVALE"
}, {
	"name": "RADICAL"
}, {
	"name": "RELIANT"
}, {
	"name": "RENAULT"
}, {
	"name": "REVA"
}, {
	"name": "RIMAC"
}, {
	"name": "RINSPEED"
}, {
	"name": "RODING"
}, {
	"name": "ROEWE"
}, {
	"name": "ROLLS-ROYCE"
}, {
	"name": "ROSSIN-BERTIN"
}, {
	"name": "ROSSION"
}, {
	"name": "ROVER"
}, {
	"name": "SAAB"
}, {
	"name": "SALEEN"
}, {
	"name": "SAIC-GM-WULING"
}, {
	"name": "SAIPA"
}, {
	"name": "SAKER"
}, {
	"name": "SAMSUNG"
}, {
	"name": "SAN"
}, {
	"name": "SBARRO"
}, {
	"name": "SCION"
}, {
	"name": "SEAT"
}, {
	"name": "SHANGHAI MAPLE"
}, {
	"name": "SIN"
}, {
	"name": "ŠKODA"
}, {
	"name": "SMART"
}, {
	"name": "SPADA VETTURE SPORT"
}, {
	"name": "SPYKER"
}, {
	"name": "SSANGYONG"
}, {
	"name": "SSC NORTH AMERICA"
}, {
	"name": "STREET & RACING TECHNOLOGY"
}, {
	"name": "SUBARU"
}, {
	"name": "SUZUKI"
}, {
	"name": "TANOM"
}, {
	"name": "TATA"
}, {
	"name": "TAURO"
}, {
	"name": "TAWON CAR"
}, {
	"name": "TD CARS"
}, {
	"name": "TESLA"
}, {
	"name": "THAI RUNG"
}, {
	"name": "TOYOTA"
}, {
	"name": "TREKKA"
}, {
	"name": "TRIDENT"
}, {
	"name": "TRIUMPH"
}, {
	"name": "TROLLER"
}, {
	"name": "TRUMPCHI"
}, {
	"name": "TUSHEK"
}, {
	"name": "TVR"
}, {
	"name": "TVS"
}, {
	"name": "ULTIMA"
}, {
	"name": "UMM"
}, {
	"name": "UEV"
}, {
	"name": "URI"
}, {
	"name": "UAZ"
}, {
	"name": "VAUXHALL MOTORS"
}, {
	"name": "VECTOR"
}, {
	"name": "VENCER"
}, {
	"name": "VENIRAUTO"
}, {
	"name": "VENTURI"
}, {
	"name": "VEPR"
}, {
	"name": "VOLKSWAGEN"
}, {
	"name": "VOLVO"
}, {
	"name": "VINFAST"
}, {
	"name": "W MOTORS"
}, {
	"name": "WALLYSCAR"
}, {
	"name": "WESTFIELD"
}, {
	"name": "WHEEGO"
}, {
	"name": "WIESMANN"
}, {
	"name": "XENIA"
}, {
	"name": "YES!"
}, {
	"name": "YOUABIAN PUMA"
}, {
	"name": "ZASTAVA AUTOMOBILES"
}, {
	"name": "ZENDER CARS"
}, {
	"name": "ZENOS CARS"
}, {
	"name": "ZENVO"
}, {
	"name": "ZIL"
}, {
	"name": "ZX AUTO"
}];

export const permisosFull = function(admin) {
  return [
    {
      id: "consulta",
      categoria: "Consultas",
      descripcion: "Consulta de datos de líneas",
      permisos: [
        {
          valor: admin ? "ADM_CONS_DATOS_LINEA" : "CONS_DATOS_LINEA",
          descripcion: "Datos Registrados del Usuario"
        },
        {
          valor: admin ? "ADM_CONS_GRUPO_FACT" : "CONS_GRUPO_FACT",
          descripcion: "Grupo de facturación"
        },
        {
          valor: admin ? "ADM_CONS_FACTURAS" : "CONS_FACTURAS",
          descripcion: "Facturas"
        },
        {
          valor: admin ? "ADM_CONS_EST_SOL" : "CONS_EST_SOL",
          descripcion: "Estado de solicitudes"
        },
        {
          valor: admin ? "ADM_CONS_FINAN " : "CONS_FINANCIACIONES",
          descripcion: "Financiaciones"
        },
        {
          valor: admin ? "ADM_CONS_FORM_DOC" : "CONS_FORMS_DOCS",
          descripcion: "Formularios y documentación"
        },
        {
          valor: admin ? "ADM_CONS_DET_CONS" : "CONS_DET_CONSUMO",
          descripcion: "Detalles de Consumo"
        },
        {
          valor: admin ? "ADM_BACKUP_AGENDA" : "BACKUP_AGENDA",
          descripcion: "Backup agenda"
        }
      ]
    },
    {
      id: "gerenciamiento",
      categoria: "Modificación",
      descripcion: "Gerenciamiento de servicios",
      permisos: [
        {
          valor: admin ? "ADM_MODIF_DATOS_USU" : "MODIF_DATOS_USUARIO",
          descripcion: "Modificación de datos de Usuario"
        },
        {
          valor: admin ? "ADM_MODIF_GRP_FACT" : "MODIF_GRUPO_FACT",
          descripcion: "Modificación Grupo Facturación"
        },
        {
          valor: admin ? "ADM_MODIF_PLAN" : "MODIF_PLAN",
          descripcion: "Cambio de Plan"
        },
        {
          valor: admin ? "ADM_MODIF_SUSP_HAB" : "MODIF_SUSP_HABILIT",
          descripcion: "Realizar suspensiones y habilitaciones"
        },
        {
          valor: admin ? "ADM_MODIF_PLAN_INET" : "MODIF_PLAN_INET",
          descripcion: "Cambio de plan internet"
        },
        {
          valor: admin ? "ADM_ACTIV_LINEA" : "ACTIV_LINEA_NUEVA",
          descripcion: "Activación de líneas nuevas"
        },
        {
          valor: admin ? "ADM_MODIF_TERMINAL" : "MODIF_TERMINAL",
          descripcion: "Cambio de terminales"
        },
        {
          valor: admin ? "ADM_MODIF_ALIAS_MAIL" : "MODIF_ALIAS_MAIL",
          descripcion: "Cambio de alias de correo"
        },
        {
          valor: admin ? "ADM_MODIF_BACKTONE" : "MODIF_BACKTONE",
          descripcion: "Administración de Backtones"
        },
        {
          valor: admin ? "ADM_ACC_CLUB_PERS" : "ACC_CLUB_PERSONAL",
          descripcion: "Club Personal"
        }
      ]
    },
    {
      id: "activacion",
      categoria: "Activación",
      descripcion: "Activación de servicios",
      permisos: [
        {
          valor: admin ? "ADM_ACTIV_TRANS_LLA" : "ACTIV_TRANS_LLAMADAS",
          descripcion: "Transferencia de llamadas"
        },
        {
          valor: admin ? "ADM_ACTIV_ROAMING" : "ACTIV_ROAMING",
          descripcion: "Roaming"
        },
        {
          valor: admin ? "ADM_ACTIV_IMP_FACT" : "ACTIV_IMP_FACT_ELEC",
          descripcion: "Habilitar Factura Digital"
        },
        {
          valor: admin ? "ADM_ACTIV_RECARGA" : "ACTIV_RECARGA_FACT",
          descripcion: "Habilitar Recarga contra Factura"
        },
        {
          valor: admin ? "ADM_ACTIV_DEST_GRAT" : "ACTIV_DEST_GRATIS",
          descripcion: "Alta y baja de destinos Gratuitos"
        }
      ]
    },
    {
      id: "gestion",
      categoria: "Gestión",
      descripcion: "Gestión del saldo",
      permisos: [
        {
          valor: admin ? "ADM_MODIF_LIM_CONS" : "MODIF_LIMITE_CONS",
          descripcion: "Aumento del límite de consumo"
        },
        {
          valor: admin ? "ADM_ACTIV_TRANS_SALD" : "ACTIV_TRANS_SALDO",
          descripcion: "Transferencia de saldo"
        },
        {
          valor: admin ? "ADM_ACC_REC_TARJ" : "ACC_REC_TARJETA",
          descripcion: "Recarga contra Tarjeta"
        },
        {
          valor: admin ? "ADM_ACTIV_RECARGA" : "PEDI_SALDO",
          descripcion: "Pedí Saldo"
        },
        {
          valor: admin ? "ADM_PRESTA_SALDO" : "PRESTA_SALDO",
          descripcion: "Presta Saldo"
        },
        {
          valor: admin ? "ADM_RECARGA_OP" : "RECARGA_FACT_OP",
          descripcion: "Recarga contra Factura(Operación)"
        },
        {
          valor: admin ? "ADM_ACTIV_PACK" : "ACTIV_PACK",
          descripcion: "Compra de Packs"
        }
      ]
    }
  ];
};
