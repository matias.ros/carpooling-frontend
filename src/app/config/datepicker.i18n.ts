import {OwlDateTimeIntl} from 'ng-pick-datetime';

export class SpanishIntl extends OwlDateTimeIntl {
  /** A label for the up second button (used by screen readers).  */
  upSecondLabel = 'agregar un segundo';

  /** A label for the down second button (used by screen readers).  */
  downSecondLabel = 'restar un segundo';

  /** A label for the up minute button (used by screen readers).  */
  upMinuteLabel = 'agregar un minuto';

  /** A label for the down minute button (used by screen readers).  */
  downMinuteLabel = 'restar un minuto';

  /** A label for the up hour button (used by screen readers).  */
  upHourLabel = 'agregar una hora';

  /** A label for the down hour button (used by screen readers).  */
  downHourLabel = 'restar una hora';

  /** A label for the previous month button (used by screen readers). */
  prevMonthLabel = 'anterior';

  /** A label for the next month button (used by screen readers). */
  nextMonthLabel = 'siguiente';

  /** A label for the previous year button (used by screen readers). */
  prevYearLabel = 'año anterior';

  /** A label for the next year button (used by screen readers). */
  nextYearLabel = 'año siguiente';

  /** A label for the previous multi-year button (used by screen readers). */
  prevMultiYearLabel = 'Previous 21 years';

  /** A label for the next multi-year button (used by screen readers). */
  nextMultiYearLabel = 'Next 21 years';

  /** A label for the 'switch to month view' button (used by screen readers). */
  switchToMonthViewLabel = 'Change to month view';

  /** A label for the 'switch to year view' button (used by screen readers). */
  switchToMultiYearViewLabel = 'Choose month and year';

  /** A label for the cancel button */
  cancelBtnLabel = 'Cancelar';

  /** A label for the set button */
  setBtnLabel = 'Confirmar';

  /** A label for the range 'from' in picker info */
  rangeFromLabel = 'Desde';

  /** A label for the range 'to' in picker info */
  rangeToLabel = 'Hasta';

  /** A label for the hour12 button (AM) */
  hour12AMLabel = 'AM';

  /** A label for the hour12 button (PM) */
  hour12PMLabel = 'PM';
}
