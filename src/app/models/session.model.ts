
export class SessionModel {
  access_token: string;
  expires_in: number;
  scope: string;
  state: string;
  token_type: string;
}
