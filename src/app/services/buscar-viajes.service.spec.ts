import { TestBed } from '@angular/core/testing';

import { BuscarViajesService } from './buscar-viajes.service';

describe('BuscarViajesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuscarViajesService = TestBed.get(BuscarViajesService);
    expect(service).toBeTruthy();
  });
});
