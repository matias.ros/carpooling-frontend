import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";

@Injectable({
  providedIn: 'root'
})
export class BuscarViajesService {
 
  private buscarUrl = Servers.Carpooling.baseUrl + "viaje/buscar-viaje";
  private detalleUrl = Servers.Carpooling.baseUrl + "viaje/detalles-viaje";
  private solicitudesUrl = Servers.Carpooling.baseUrl + "viaje/solicitudes-por-viaje";
  private misViajesUrl = Servers.Carpooling.baseUrl +"viaje/viajes-por-usuario";
  private viajesByConductor = Servers.Carpooling.baseUrl +"viaje/viajes-por-conductor";
  private soliViajeUrl = Servers.Carpooling.baseUrl +"viaje/solicitar-viaje";
  private canceViajeUrl = Servers.Carpooling.baseUrl +"viaje/cancelar-viaje";
  
  constructor(private http: HttpClient) {}
   
  buscarViajes(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.buscarUrl, body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }
  reservarViaje(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.soliViajeUrl, body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }
  cancelarViaje(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.canceViajeUrl, body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }
  getDataViaje(id): Observable<any> {
    return this.http
      .get<any[]>(this.detalleUrl + "?id=" + id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  listarTodosMisViajes(): Observable<any> {
    return this.http
      .get<any[]>(this.misViajesUrl, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  listarViajesConductor(id): Observable<any> {
    return this.http
      .get<any[]>(this.viajesByConductor + "?id=" + id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  listarSolicitudes(id): Observable<any> {
    return this.http
      .get<any[]>(this.solicitudesUrl+ "?id=" + id, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }
  
}
