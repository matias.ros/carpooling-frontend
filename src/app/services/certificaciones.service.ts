import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";

@Injectable({
  providedIn: "root"
})
export class CertificacionesService {
  constructor(private http: HttpClient) {}
}
