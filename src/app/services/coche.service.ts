import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";

@Injectable({
  providedIn: "root"
})
export class CocheService {
  private cocheUrl = Servers.Carpooling.baseUrl + "auto/";

  constructor(private http: HttpClient) {}

  listarCoches(): Observable<any> {
    return this.http.get<any[]>(this.cocheUrl + "listar-autos").pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  registrarCoche(data): Observable<any> {
    let body = data;
    console.log(this.cocheUrl + "agregar-auto");
    return this.http
      .post<any[]>(this.cocheUrl + "agregar-auto", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  checkChapa(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.cocheUrl + "validar-chapa", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  subirFotoCoche(data): Observable<any> {
    let body = data;
    console.log(this.cocheUrl + "agregar-foto");
    return this.http
      .post<any[]>(this.cocheUrl + "agregar-foto", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  listarCochesActivos(): Observable<any> {
    return this.http.get<any[]>(this.cocheUrl + "listar-autos-activos").pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  listarMarcas(): Observable<any> {
    return this.http.get<any[]>(Servers.Carpooling.baseUrl + "marca").pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  desactivarCoche(data): Observable<any> {
    return this.http
      .get<any[]>(this.cocheUrl + "desactivar-vehiculo?id=" + data.id)
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }

  activarCoche(data): Observable<any> {
    return this.http
      .get<any[]>(this.cocheUrl + "activar-vehiculo?id=" + data.id)
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }
}
