import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  formateoFecha(date){
    var newDate = null;
    date = new Date(date);
    if(date){
      newDate = ("0" + (date.getDate())).slice(-2) +'/'+("0" + (date.getMonth()+1)).slice(-2)+'/'+date.getFullYear() + " ";
      newDate = newDate + ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2) + ":" + ("0" + (date.getSeconds())).slice(-2);
    }
    return newDate;
   }
   getOnlyTime(date){
    var newDate = null;
    date = new Date(date);
    if(date){
      newDate = ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2) ;
    }
    return newDate;
   }
   getOnlyDate(date){
    var newDate = null;
    date = new Date(date);
    if(date){
      newDate = ("0" + (date.getDate())).slice(-2) +'/'+("0" + (date.getMonth()+1)).slice(-2)+'/'+date.getFullYear();
    }
    return newDate;
   }

   getDate(date){
    var newDate = null;
    date = new Date(date);
    if(date){
      newDate = ("0" + (date.getDate())).slice(-2) +'/'+("0" + (date.getMonth()+1)).slice(-2)+'/'+date.getFullYear() + "";
    }
    return newDate;
   }

   getDateT(date){
    var newDate = null;
    if(date){
      newDate =  date.getFullYear() +'-'+("0" + (date.getMonth()+1)).slice(-2)+'-'+("0" +(date.getDate())).slice(-2) + "T";
      newDate = newDate + ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2) + ":" + ("0" + (date.getSeconds())).slice(-2) +"Z";
    }
    return newDate;
   }

   getMMDDYYYY(date){
    var newDate = null;
    if(date){
      console.log(date)
      newDate = ("0" + (date.getMonth()+1)).slice(-2)+'-'+("0" +(date.getDate())).slice(-2)+ "-" +  date.getFullYear();
    }
    return newDate;
   }
   getFullDate(date){
     var newDate = null;
     date = new Date(date)
     if(date){
        newDate = date;
     }
     return newDate;
   }
}