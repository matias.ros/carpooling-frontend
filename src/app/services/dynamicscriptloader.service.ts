import { Injectable } from "@angular/core";

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  { name: "theme", src: "assets/js/theme.js" },
  { name: "jquery", src: "../../assets/js/jquery.min.js" },
  { name: "countdown", src: "../../assets/js/jquery.countdown.min.js" },
  { name: "rangeSlider", src: "../../assets/js/ion.rangeSlider.min.js" },
  { name: "jarallax-element", src: "assets/js/jarallax-element.min.js" },
  { name: "jarallax", src: "assets/js/jarallax.min.js" } 
];
declare var document: any;

@Injectable({
  providedIn: "root"
})
export class DynamicscriptloaderService {
  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  load(...scripts: string[]) {
    const promises: any[] = [];
    scripts.forEach(script => promises.push(this.loadScript(script)));
    return Promise.all(promises);
  }

  loadScript(name: string) {
    return new Promise((resolve, reject) => {
      //load script
      let script = document.createElement("script");
      script.type = "text/javascript";
      script.src = this.scripts[name].src;
      if (script.readyState) {
        //IE
        script.onreadystatechange = () => {
          if (
            script.readyState === "loaded" ||
            script.readyState === "complete"
          ) {
            script.onreadystatechange = null;
            this.scripts[name].loaded = true;
            resolve({ script: name, loaded: true, status: "Loaded" });
          }
        };
      } else {
        //Others
        script.onload = () => {
          this.scripts[name].loaded = true;
          resolve({ script: name, loaded: true, status: "Loaded" });
        };
      }
      script.onerror = (error: any) =>
        resolve({ script: name, loaded: false, status: "Loaded" });
      document.getElementsByTagName("body")[0].appendChild(script);
    });
  }
}
