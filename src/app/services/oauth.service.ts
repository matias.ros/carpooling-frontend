import { Injectable } from '@angular/core';
import { Conf,Servers } from '../config/api';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { handleError } from '../util/error-handler';


@Injectable({
  providedIn: 'root'
})
export class OauthService {
  private authUrl = Servers.Carpooling.baseUrl + "sesion/login";
  private logoutUrl = Servers.Carpooling.baseUrl + "sesion/logout";
  constructor(private http: HttpClient) { }

  loginOauth(data): Observable<any> {
    let body = data
    console.log(this.authUrl)
    return this.http.post<any[]>(this.authUrl,
      body, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .pipe(
        tap((response: any) => {
          if (response) {
            console.log("response", response)
            //localStorage.setItem('currentUser', JSON.stringify(response));
          } else if (response.code === 302) {
            response.error = true;
            response.message = 'No se obtuvieron las credenciales';
          } else {
            response.error = true;
            response.message = 'No se obtuvieron las credenciales';
          }
        }),
        catchError(handleError('loginAuth', {}))
      );
  }

  getOauthData() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  logout() {
    return this.http.post<any[]>(this.logoutUrl,
      '', {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .pipe(
        tap((response: any) => {
          console.log(response)
        }),
        catchError(handleError('loginAuth', {}))
      );
  }

}
