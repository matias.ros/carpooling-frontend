import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";

@Injectable({
  providedIn: "root"
})
export class PreferenciasService {
  private preferenciaUrl = Servers.Carpooling.baseUrl + "preferencia/";
  constructor(private http: HttpClient) {}

  listarPreferencias(): Observable<any> {
    return this.http
      .get<any[]>(this.preferenciaUrl + "obtener-preferencia-usuario")
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }

  setPreferencias(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.preferenciaUrl + "agregar-preferencia", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  updatePreferencias(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.preferenciaUrl + "actualizar-preferencia", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }
}
