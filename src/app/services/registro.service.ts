import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";

@Injectable({
  providedIn: "root"
})
export class RegistroService {
  private authUrl = Servers.Carpooling.baseUrl + "miembro/registro";
  private regViaje = Servers.Carpooling.baseUrl + "viaje/agendar-viaje";
  private miembroUrl = Servers.Carpooling.baseUrl + "miembro/";
  private smsUrl = Servers.Carpooling.baseUrl + "validar/";

  constructor(private http: HttpClient) {}

  registrarUsuario(data): Observable<any> {
    let body = data;
    console.log(this.authUrl);
    return this.http
      .post<any[]>(this.authUrl, body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("loginAuth", {}))
      );
  }

  registrarViaje(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.regViaje, body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        })
      );
  }

  existenciaCorreo(data): Observable<any> {
    let correo = data;
    return this.http
      .get<any[]>(this.miembroUrl + "existe-correo?correo=" + data)
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }

  enviarSMS(data): Observable<any> {
    let numero = data;
    return this.http
      .get<any[]>(this.smsUrl + "envio-mensaje?numero=" + data)
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }

  enviarCorreo(): Observable<any> {
    return this.http.get<any[]>(this.smsUrl + "reenviar-correo").pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  verificacionCodigo(data): Observable<any> {
    let numero = data;
    return this.http.get<any[]>(this.smsUrl + "numero?clave=" + data).pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  verCertificaciones(): Observable<any> {
    return this.http.get<any[]>(this.smsUrl + "verificar").pipe(
      tap((response: any) => {
        console.log(response);
      }),
      catchError(handleError("codigoMensaje", {}))
    );
  }

  certificarFacebook(data): Observable<any> {
    let body = data;
    return this.http
      .post<any[]>(this.smsUrl + "validar-facebook", body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        })
      );
  }

  getOauthData() {
    return JSON.parse(localStorage.getItem("currentUser"));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
  }
}
