import { Injectable } from "@angular/core";
import { Conf, Servers } from "../config/api";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { handleError } from "../util/error-handler";
@Injectable({
  providedIn: 'root'
})
export class SolicitudesService {

  private solicitudesUrl = Servers.Carpooling.baseUrl + "viaje/listar-solicitudes";
  private aprobarSolicitudesUrl = Servers.Carpooling.baseUrl + "viaje/aprobar-solicitud";
  private rechazarSolicitudesUrl = Servers.Carpooling.baseUrl + "viaje/rechazar-solicitud";
  constructor(private http: HttpClient) { }

  listarSolicitudes(): Observable<any> {
    return this.http
      .get<any[]>(this.solicitudesUrl)
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }
  aprobarSolicitud(data): Observable<any> {
    var body =data
    return this.http
      .post<any[]>(this.aprobarSolicitudesUrl,body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }
  rechazarSolicitud(data): Observable<any> {
    var body = data;
    return this.http
      .post<any[]>(this.rechazarSolicitudesUrl,body, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .pipe(
        tap((response: any) => {
          console.log(response);
        }),
        catchError(handleError("codigoMensaje", {}))
      );
  }
}
