import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {OauthService} from '../services/oauth.service';
import {Router} from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: OauthService, private router: Router) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((response: any) => {
        if (response.ok && response.status === 200){
          if (response.body){
            if(!!response.body.codigo && response.body.codigo !== 0 && response.body.codigo !== '0'){
              return throwError(response);
            }else if (response.body.exitoso === false){
              return throwError(response);
            }
          }
        }
        return response;
      }),
      catchError(err => {
      if (err.status === 401) {
        // auto logout if 401 response returned from api
        this.authenticationService.logout();
        this.router.navigate(['login', {}])
      }

      return throwError(err);
    }))
  }
}
