import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {OauthService} from '../services/oauth.service';
import {permisosVistas} from '../config/constants';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public oauth: OauthService, public router: Router) {}
  canActivate( next: ActivatedRouteSnapshot,
               state: RouterStateSnapshot): boolean {
    let currentUser = this.oauth.getOauthData();
    let route = state.url.split(';')[0];
    if (currentUser && currentUser.scope){
      if(permisosVistas[route]){
        let hasPermissions = true;
        for (let permiso of permisosVistas[route]){
          if (!currentUser.scope.includes(permiso)){
            hasPermissions = false;
          }
        }
        if (hasPermissions) {
          return true;
        }
      }else {
        return true;
      }
    }
    this.router.navigate(['login']);
    return false;
  }
}
