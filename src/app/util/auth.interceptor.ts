import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentUser = JSON.parse(localStorage.getItem('currentUser')); 
    if (currentUser && currentUser.tokenHash) {
      let _ = null;
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.tokenHash}`,
          aplicacion: 'carpooling'
        }
      });
    }else {
      request = request.clone({
        setHeaders: {
          aplicacion: 'carpooling'

        }
      });
    }

    return next.handle(request);
  }
}
