import {Observable, of} from 'rxjs';

export function handleError<T> (operationType = 'operation', result?: any) {
  return (error: any): Observable<any> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    let message = 'Ha ocurrido un error indeterminado.';
    let code = error.status;

    if (operationType === 'loginAuth'){
      message = error.error.error_description ? error.error.error_description: message;
    }else if (operationType === 'codigoMensaje'){
      message = error.error.mensaje;
      code = error.error.codigo;
    }else if (operationType === 'default'){
      if (error.error && error.error.message && error.error.code){
        message = error.error.message;
        code = error.error.code;
      }
    }
    // Let the app keep running by returning an empty result.
    return of({error: true, code, message});
  };
}
