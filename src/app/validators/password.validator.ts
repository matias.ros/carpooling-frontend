import {AbstractControl} from '@angular/forms';
export class PasswordValidator {
  /** Cadenas en minusculas */
  static LOWER = /[a-z]/;
  /** Cadenas en mayusculas */
  static UPPER = /[A-Z]/;
  /** Cadenas con un digito. */
  static DIGIT = /[0-9]/;
  /** Cadenas con más de un digito. */
  static DIGITS = /[0-9].*[0-9]/;
  /** Cadenas con caracteres especiales */
  static SPECIAL = /[^a-zA-Z0-9]/;
  /** Cadenas con el mismo caracter. */
  static SAME = /^(.)\1+$/;
  //los mensajes asociados a la contraseña
  static messages = {
    "similar-to-username": "Contiene palabras que están contenidas en tu email",
    "empty": "La contraseña es requerida",
    "too-short": "Demasiado Corto",
    "very-weak": "Muy débil",
    "weak": "Débil",
    "good": "Bueno",
    "strong": "Fuerte"
  };

  static type = {
    "similar-to-username": "danger",
    "empty": "info",
    "too-short": "danger",
    "very-weak": "danger",
    "weak": "warning",
    "good": "info",
    "strong": "success"
  };

  /**
   * Esta función se encarga de construir el objeto que será retornado.
   * @function
   *
   * @private
   * @author <a href="mailto:mbaez@konecta.com.py">Maximiliano Báez</a>
   * @param rate {Integer} Un valor entre 0 y 4 que idndica el rating.
   * @param key {String} La clave del mensaje.
   * @returns {Integer} rate : Un valor entre 0 y 4 que idndica el rating
   *              del password.
   * @returns {String} key : La clave del mensaje.
   * @returns {String} message : El mensaje asociado a la contraseña.
   */
  static rating(rate, key) {
    return {
      rate: rate,
      key: key,
      message: this.messages[key],
      type: this.type[key]
    };
  };


  static MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if(password != confirmPassword) {
      AC.get('confirmPassword').setErrors( {MatchPassword: true} )
    } else {
      return null
    }
  }
  /**
   * Esta función se encarga de transformar la cadena, de forma que la
   * primera letra este en minuscula.
   * @function
   *
   * @private
   * @author <a href="mailto:mbaez@konecta.com.py">Maximiliano Báez</a>
   * @param str {String} La cadena a transformar.
   * @returns {String} La cadena transformada con el método.
   */

  static uncapitalize(str) {
    return str.substring(0, 1).toLowerCase() + str.substring(1);
  };

  static validator(password, username) {
    //se definen las expresiones regulares
    console.log(password,username,username.toLowerCase().includes(password.toLowerCase()))

    
    if (!password || password.length == 0) {
      //se verifica la longitud de la contraseña
      return this.rating(-1, "empty");
    }
    if (!password || password.length < 8) {
      //se verifica la longitud de la contraseña
      return this.rating(0, "too-short");
    } else if (username && username.toLowerCase().includes(password.toLowerCase())) {
      //se verifica si la contraseña no contiene el username.
      return this.rating(0, "similar-to-username");
    } else if (this.SAME.test(password)) {
      //se verifica que la cadena no este compuesta por el mismo caracter.
      return this.rating(1, "very-weak");
    }
    // se evalua la contraseña con ayuda de las expresiones regulares
    // definidas anteriormente.
    var lower = this.LOWER.test(password);
    var upper = this.UPPER.test(this.uncapitalize(password));
    var digit = this.DIGIT.test(password);
    var digits = this.DIGITS.test(password);
    var special = this.SPECIAL.test(password);

    if (lower && upper && digit || lower && digits || upper && digits || special) {
      //se verifica si la contraseña es fuerte
      return this.rating(4, "strong");
    } else if (lower && upper || lower && digit || upper && digit) {
      //se verifica si la contraseña es buena.
      return this.rating(3, "good");
    }
    //la contraseña es débil.
    return this.rating(2, "weak");
  }

}
